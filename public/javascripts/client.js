
var serverURL = 'http://localhost:3000';

//var io = require('socket.io');
var server = io.connect(serverURL);

console.log('Test');

server.on('connect', function(data) {
	//server.emit('playGame', '');
	var username = get_sent_data()['nombre_s'];
	console.log('Test user '+username+' connected');
	server.emit('userConnected', username);//, users, args)

});

function get_sent_data(){
	// funcion creada para obtener la informacion en get
	var loc = document.location.href;
	// si existe el interrogante
	if(loc.indexOf('?')>0)
	{
		// cogemos la parte de la url que hay despues del interrogante
		var getString = loc.split('?')[1];
		// obtenemos un array con cada clave=valor
		var GET = getString.split('&');
		var get = {};
		// recorremos todo el array de valores
		for(var i = 0, l = GET.length; i < l; i++){
			var tmp = GET[i].split('=');
			get[tmp[0]] = unescape(decodeURI(tmp[1]));
		}
		return get;
	}
}

server.on('MsgReceived', function(data){


	var t = new Date(),
		curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
		curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
		curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
	var time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;

	var cl = $('.cards');
	var name = 'Author';
	cl.append('<li class=\'card\'><div class=\'commentText comment\'><p class=\'\'><strong>'+data+'</strong></p></div></li>');
	$('.cards').commentCards();
	cl.animate({ scrollTop: cl.prop('scrollHeight')}, 100);

	$('.comment').each(function(i, d){
		$(d).emoji();
	});

	//setTimeout(function(){
	//    server.emit('phraseRead', '');
	//}, 2000);


	//$(".ChatBox").val() = $(".ChatBox").val() + '\n' + data;

});

server.on('question', function(data) {

});

server.on('variables', function(data){

});

server.on('updateGame', function(data){

});

server.on('updateGame', function(data){

});


server.on('complete', function(data) {
    $(".card--current >  div").children().text("");
    $(".card--current >  div> p:nth-child(1)").html("<strong>Felicidades, has completado la sesión de argumentación colaborativa</strong>");
    $(".padding:not(.hide)").addClass("hide");
});

server.on('wait', function(data) {

    $(".card--current >  div").children().text("");
    $(".card--current >  div> p:nth-child(1)").html("<strong> Por favor espera a tus compañeros</strong>");
    $(".padding:not(.hide)").addClass("hide");
	//alert('Por favor espera a tus compañeros');
});


server.on('Qu', function(qu) {
	console.log('client received qu type ' + qu.type+' with arg id '+qu.argID);

	var t = new Date(),
		curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
		curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
		curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
	var time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;

	console.log(qu);
	$('.question-text').text(qu.topicText.split("#")[0]);
    $('.position-1').text(qu.topicText.split("#")[1]);
    $('.position-2').text(qu.topicText.split("#")[2]);
    $('.position-3').text(qu.topicText.split("#")[3]);
	$('.question-text-bottom').text(qu.topicText.split('#')[qu.topicText.split('#').length-1]);
	$('.selected-image-background').css('background-image', 'url("../images/'+qu.imageName+'.jpg")');
	$('.selected-image-src').attr('src','../images/'+qu.imageName+'.jpg');

	switch(qu.type) {

	case 1:
		console.log('QUWAS',qu);
		$('.SendMsgBtn1Y').off();
		$('.SendMsgBtn1N').off();
		$('.SendMsgBtn1N1').off();
		$('.openchat').addClass('hide');
		$('.q1chat').removeClass('hide');
		$('.q2achat').addClass('hide');
		$('.q2bchat').addClass('hide');
		$('.q3achat').addClass('hide');
		$('.q3bchat').addClass('hide');
		$('.q4achat').addClass('hide');
		$('.q4bchat').addClass('hide');
		$('.q5achat').addClass('hide');
		$('.q5bchat').addClass('hide');


		var cl = $('.cards');
		var name = 'Author';
		var preg = '¿Qué opinas de este argumento?';
		if(qu.ancestors.length>2) {
            cl.append('<li class=\'card\'><div class=\'commentText comment\'><p>' + qu.argText + '</p><p><strong>' + preg + '</strong></p><button class=\'answer specialBtn details\'><strong>Ver detalle</strong></button></div></li>');
        }
        else{
            cl.append('<li class=\'card\'><div class=\'commentText comment\'><p>' + qu.argText + '</p><p><strong>' + preg + '</strong></p></div></li>');
		}
        $('.cards').commentCards();
        $('.modal-body-custom ul').children().remove();
		for(i=qu.ancestors.length-2; i>=0;i--){

			if(i==0){
				$('.modal-body-custom ul').append('<li><h8 class="without">'+qu.ancestors[i]["text"]+'</h8> </li>');
            }
			else if(qu.ancestors[i-1]["type"]==2){
            	$('.modal-body-custom ul').append('<li class="green"><input type="checkbox" checked><h8>'+qu.ancestors[i]["text"]+'</h8> <p>Tiene el siguiente argumento a favor</p> </li>');
			}
			else if(qu.ancestors[i-1]["type"]==3){
                $('.modal-body-custom ul').append('<li class="red"><input type="checkbox" checked><h8>'+qu.ancestors[i]["text"]+'</h8> <p>Tiene el siguiente argumento en contra</p> </li>');

            }
		}



		cl.animate({ scrollTop: cl.prop('scrollHeight')}, 100);
		$('.SendMsgBtn1NI').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;
			var ans = 3;
			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(ans);

			console.log('QUIS',qu);

			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');
		});

		$('.SendMsgBtn1Y').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;
			var ans = 1;
			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(ans);

			console.log('QUIS',qu);

			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');
		});
		$('.SendMsgBtn1N').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;
			var ans = 2;
			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(ans);

			console.log('QUIS',qu);

			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');
		});
		break;
	case 2:
		console.log('QUWAS',qu);
		$('.SendMsgBtn3').off();
		$('.openchat').addClass('hide');
		$('.q1chat').addClass('hide');
		$('.q2achat').removeClass('hide');
		$('.q2bchat').addClass('hide');
		$('.q3achat').addClass('hide');
		$('.q3bchat').addClass('hide');
		$('.q4achat').addClass('hide');
		$('.q4bchat').addClass('hide');
		$('.q5achat').addClass('hide');
		$('.q5bchat').addClass('hide');
		var reasoningList = $('.rlist1');
		reasoningList.empty();
		for(var i = 0; i < qu.childrenText.length; i++)
		{
			if(qu.childrenType[i] == 2)
				reasoningList.append('<li class="answer list-group-item" data-id='+qu.childrenID[i]+'>'+qu.childrenText[i]+'</li>');
		}
		f();
		var cl = $('.cards');
		var name = 'Author';
		var preg = '¿Tienes alguna justificación para estar de acuerdo con este argumento?';
		cl.append('<li class=\'card\'><div class=\'commentText comment\'><p>'+qu.argText+'</p><p><strong>'+preg+'</strong></p></div></li>');
		$('.cards').commentCards();
		cl.animate({ scrollTop: cl.prop('scrollHeight')}, 100);

		$('.SendMsgBtn3').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;

			var ans = $('input[name=question2]:checked').val();
			var checkedItems = {}, counter = 0;

			// Code here to append first arg to response
			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(-1);

			$('.list-group-item.list-group-item-primary.active').each(function(idx, li) {
				qu.respArgID.push($(li).data('id'));
				qu.respArgText.push($(li).text());
				qu.respArgVote.push(1);
				//  cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong> "+qu.respArgText[qu.respArgText.length-1]+"</p> <span class='date sub-text'>"+time+"</span></div></li>");
				//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
			});

			console.log('QUIS',qu);

			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
			$('input[type=radio]').attr('checked', false);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');


		});
		break;
	case 3:
		console.log('QUWAS',qu);
		$('.SendMsgBtn5').off();
		$('.openchat').addClass('hide');
		$('.q1chat').addClass('hide');
		$('.q2achat').addClass('hide');
		$('.q2bchat').addClass('hide');
        $('.q3achat').removeClass('hide');
		$('.q3bchat').addClass('hide');
		$('.q4achat').addClass('hide');
		$('.q4bchat').addClass('hide');
		$('.q5achat').addClass('hide');
		$('.q5bchat').addClass('hide');
		var reasoningList = $('.rlist2');
		reasoningList.empty();
		for(var i = 0; i < qu.childrenText.length; i++)
		{
			if(qu.childrenType[i] == 3)
				reasoningList.append('<li class="answer list-group-item" data-id='+qu.childrenID[i]+'>'+qu.childrenText[i]+'</li>');
		}
		f();
		var cl = $('.cards');
		var name = 'Author';
		var preg = '¿Tienes alguna justificación para estar en desacuerdo con este argumento?';
		cl.append('<li class=\'card\'><div class=\'commentText comment\'><p>'+qu.argText+'</p><p><strong>'+preg+'</strong></p></div></li>');
		$('.cards').commentCards();
		cl.animate({ scrollTop: cl.prop('scrollHeight')}, 100);

		$('.SendMsgBtn5').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;

			var ans = $('input[name=question3]:checked').val();
			var checkedItems = {}, counter = 0;

			// Code here to append first arg to response
			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(-1);

			$('.list-group-item.list-group-item-primary.active').each(function(idx, li) {
				qu.respArgID.push($(li).data('id'));
				qu.respArgText.push($(li).text());
				qu.respArgVote.push(2);
				//  cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong> "+qu.respArgText[qu.respArgText.length-1]+"</p> <span class='date sub-text'>"+time+"</span></div></li>");
				//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
			});

			console.log('QUIS',qu);

			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
			$('input[type=radio]').attr('checked', false);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');

		});
		break;
	case 4:
		$('.SendMsgBtn6').off();
		$('.openchat').addClass('hide');
		$('.q1chat').addClass('hide');
		$('.q2achat').addClass('hide');
		$('.q2bchat').addClass('hide');
		$('.q3achat').addClass('hide');
		$('.q3bchat').addClass('hide');
		$('.q4achat').removeClass('hide');
		$('.q4bchat').addClass('hide');
		$('.q5achat').addClass('hide');
		$('.q5bchat').addClass('hide');
		var reasoningList = $('.rlist3');
		reasoningList.empty();
		for(var i = 0; i < qu.childrenText.length; i++)
		{
			if(qu.childrenType[i] == 2)
				reasoningList.append('<li class="answer list-group-item" data-id='+qu.childrenID[i]+'>'+qu.childrenText[i]+'</li>');
		}
		f();
		var cl = $('.cards');
		var name = 'Author';
		var preg = '¿Por qué estás de acuerdo con '+qu.argText+' si estás de acuerdo con el siguiente argumento en contra?';
		for(var i = 0; i < qu.childrenType.length; i++)
		{
			if(qu.childrenType[i] == 3 && qu.childrenVote[i] == 1)
			{
				preg += ' '+qu.childrenText[i]+' ';
			}
		}
		cl.append('<li class=\'card\'><div class=\'commentText comment\'><p><strong>'+preg+'</strong></p><p>'+qu.argText+'</p></div></li>');
		$('.cards').commentCards();
		cl.animate({ scrollTop: cl.prop('scrollHeight')}, 100);
		$('.SendMsgBtn6').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;

			var ans = $('input[name=question4]:checked').val();
			var checkedItems = {}, counter = 0;


			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(-1);

			// Code here to append first arg to response
			if(parseInt(ans) == 0)
			{
				// cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>No lo sé</p> <span class='date sub-text'>"+time+"</span></div></li>");
				// cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
			}
			else if(parseInt(ans) == 1){
				//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>Cometí un error, estoy en desacuerdo con el argumento principal</p> <span class='date sub-text'>"+time+"</span></div></li>");
				//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
				qu.changeIrrational='change-parent-no';
			}
			else if(parseInt(ans) == 2)
			{
				qu.changeIrrational='change-child-no';
				//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>Cometí un error, estoy en desacuerdo con el(los) contraargumento(s)</p> <span class='date sub-text'>"+time+"</span></div></li>");
				// cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
			}
			else if(parseInt(ans) == 3)
			{
				//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>Estoy de acuerdo con los siguientes argumentos:</p> <span class='date sub-text'>"+time+"</span></div></li>");
				// cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);

				$('.list-group-item.list-group-item-primary.active').each(function(idx, li) {
					qu.respArgID.push($(li).data('id'));
					qu.respArgText.push($(li).text());
					qu.respArgVote.push(1);
					//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>"+$(li).text()+"</p> <span class='date sub-text'>"+time+"</span></div></li>");
					//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
				});
			}


			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
			$('input[type=radio]').attr('checked', false);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');
		});
		break;
	case 5:
		$('.SendMsgBtn7').off();
		$('.openchat').addClass('hide');
		$('.q1chat').addClass('hide');
		$('.q2achat').addClass('hide');
		$('.q2bchat').addClass('hide');
		$('.q3achat').addClass('hide');
		$('.q3bchat').addClass('hide');
		$('.q4achat').addClass('hide');
		$('.q4bchat').addClass('hide');
		$('.q5achat').removeClass('hide');
		$('.q5bchat').addClass('hide');
		var reasoningList = $('.rlist4');
		reasoningList.empty();
		for(var i = 0; i < qu.childrenText.length; i++)
		{
			if(qu.childrenType[i] == 3)
				reasoningList.append('<li class="answer list-group-item" data-id='+qu.childrenID[i]+'>'+qu.childrenText[i]+'</li>');
		}
		f();
		var cl = $('.cards');
		var name = 'Author';
		var preg = '¿Por qué no estás de acuerdo con '+qu.argText+' si estás de acuerdo con el siguiente argumento a favor?';
		console.log('Test children' + qu.childrenType.length); // delete
		for(var i = 0; i < qu.childrenType.length; i++)
		{
			if(qu.childrenType[i] == 2 && qu.childrenVote[i] == 1)
			{
				preg += ' '+qu.childrenText[i]+' ';
			}
		}
		cl.append('<li class=\'card\'><div class=\'commentText comment\'><p><strong>'+preg+'</strong></p><p>'+qu.argText+'</p></div></li>');
		$('.cards').commentCards();
		cl.animate({ scrollTop: cl.prop('scrollHeight')}, 100);
		$('.SendMsgBtn7').click(function(event){
			event.stopImmediatePropagation();
			curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? '0' + t.getHours() : t.getHours()),
			curMinute = t.getMinutes() < 10 ? '0' + t.getMinutes() : t.getMinutes(),
			curSeconds = t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds();
			time = 'Enviado a las '+curHour+':'+curMinute+':'+curSeconds;

			var ans = $('input[name=question5]:checked').val();
			var checkedItems = {}, counter = 0;

			qu.respArgID.push(qu.argID);
			qu.respArgText.push(qu.argText);
			qu.respArgVote.push(-1);

			// Code here to append first arg to response
			if(parseInt(ans) == 0)
			{
				// cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>No lo sé</p> <span class='date sub-text'>"+time+"</span></div></li>");
				//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
			}
			else if(parseInt(ans) == 1){
				//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>Cometí un error, estoy de acuerdo con el argumento principal</p> <span class='date sub-text'>"+time+"</span></div></li>");
				//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
				qu.changeIrrational='change-parent-yes';

			}
			else if(parseInt(ans) == 2)
			{
				qu.changeIrrational='change-child-no';

				//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>Cometí un error, estoy en desacuerdo con el(los) contraargumento(s)</p> <span class='date sub-text'>"+time+"</span></div></li>");
				// cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
			}
			else if(parseInt(ans) == 3)
			{
				//cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>Estoy de acuerdo con los siguientes argumentos:</p> <span class='date sub-text'>"+time+"</span></div></li>");
				//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);

				$('.list-group-item.list-group-item-primary.active').each(function(idx, li) {
					qu.respArgID.push($(li).data('id'));
					qu.respArgText.push($(li).text());
					qu.respArgVote.push(1);
					// cl.append("<li><div class='commenterImage'><img src='"+serverURL+"/images/maxim.jpg'/></div><div class='commentText comment'><p class=''><strong>"+"Matias"+":</strong>"+$(li).text()+"</p> <span class='date sub-text'>"+time+"</span></div></li>");
					//cl.animate({ scrollTop: cl.prop("scrollHeight")}, 100);
				});
			}
			server.emit('clientResponse', qu);
			console.log('client sent response to qu type ' + qu.type);
			console.log(qu); // delete
			$('input[type=radio]').attr('checked', false);
            $('.specialBtn').addClass('disabledbutton');
            $('.NewArgBtn').removeClass('hide');
            $('.NewArgBtn2').removeClass('hide');
            $('.NewArgBtn3').removeClass('hide');
            $('.NewArgBtn4').removeClass('hide');
		});
		break;
	}
	$('.comment').each(function(i, d){
		$(d).emoji();
	});
	/*if (quType == 1){
        // Edit the response here
        var argIDR = new Array;
        var argTextR = new Array;
        var argVoteR = new Array;
        argIDR = argID;
        argTextR = argText;
        argVoteR[0] = 3;
    }
    else if (quType == 2) {

        // Correct the votes from existing args (of the correct type)
        // for each childrenID
        // fill in vote based on radio button of childrenID
        // only send back children with votes that have been changed

        // if arg has been added
        childrenID.push(-1);
        childrenType.push(2);
        childrenText.push('Added Pro Arg');
        childrenVote.push(1);
        console.log('Added Pro Arg');

        var argIDR = new Array;
        var argTextR = new Array;
        var argVoteR = new Array;
        argIDR = argID;
        argTextR = argText;
        argVoteR[0] = -1;

        // Compile response
        for (var ie = 0; ie < childrenType.length; ie++){

            argIDR.push(childrenID[ie]);
            //argTypeR.push(childrenType[i]);
            argTextR.push(childrenText[ie]);
            argVoteR.push(childrenVote[ie]);

        }

    }
    else if (quType == 3) {

        // Correct the votes from existing args (of the correct type)
        // for each childrenID
        // fill in vote based on radio button of childrenID
        // only send back children with votes that have been changed

        console.log(childrenID);
        // if arg has been added
        childrenID.push(-1);
        childrenType.push(3);
        childrenText.push('Added Con Arg');
        childrenVote.push(1);
        console.log('Added Con Arg');

        var argIDR = new Array;
        var argTextR = new Array;
        var argVoteR = new Array;
        argIDR = argID;
        argTextR = argText;
        argVoteR[0] = -1;

        // Compile response
        for (var ig = 0; ig < childrenType.length; ig++){

            argIDR.push(childrenID[ig]);
            argTextR.push(childrenText[ig]);
            argVoteR.push(childrenVote[ig]);

        }

    }

    // Compile response


    console.log(argIDR);

    console.log('client emits response to Qu type '+ quType);
    */

    $('.details').on( "click", function() {

        $('.MsgText6').val('');
        $('#myModal5').modal('show');
    });
});


$('.SendMsgBtn').click(function(){

	var strMsg = $('.MsgText').val();
	server.emit('MsgSent',strMsg);
});

$('input[name=question2]').change(function() {
    if($('input[name=question2]:checked').val() == 0){
        $('.q2bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
    }
    else{
        $('.q2bchat').addClass('hide');

        $('.specialBtn').removeClass('disabledbutton');
    }
});
$('input[name=question2]').parent().click(function() {

	$(this).children(0).prop('checked', true);
	if($('input[name=question2]:checked').val() == 0){
		$('.q2bchat').removeClass('hide');
		$('.specialBtn').addClass('disabledbutton');
	}
	else{
		$('.q2bchat').addClass('hide');

		$('.specialBtn').removeClass('disabledbutton');
	}

});


$('.SendMsgBtn3').click(function(){

});

$('input[name=question3]').change(function() {
    if($('input[name=question3]:checked').val() == 0){
        $('.q3bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
    }
    else{
        $('.q3bchat').addClass('hide');

        $('.specialBtn').removeClass('disabledbutton');
    }
});
$('input[name=question3]').parent().click(function() {
    $(this).children(0).prop('checked', true);

    if($('input[name=question3]:checked').val() == 0){
        $('.q3bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
    }
    else{
        $('.q3bchat').addClass('hide');

        $('.specialBtn').removeClass('disabledbutton');
    }
});

$('input[name=question4]').parent().change(function() {
	if($('input[name=question4]:checked').val() == 3){
		$('.q4bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
	}
	else{
		$('.q4bchat').addClass('hide');

        $('.specialBtn').removeClass('disabledbutton');
	}
});
$('input[name=question4]').parent().click(function() {
    $(this).children(0).prop('checked', true);
    if($('input[name=question4]:checked').val() == 3){
        $('.q4bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
    }
    else{
        $('.q4bchat').addClass('hide');

        $('.specialBtn').removeClass('disabledbutton');
    }
});

$('input[name=question5]').change(function() {
	if($('input[name=question5]:checked').val() == 3){
		$('.q5bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
	}
	else{
		$('.q5bchat').addClass('hide');
        $('.specialBtn').removeClass('disabledbutton');
	}
});

$('input[name=question5]').parent().click(function() {
    $(this).children(0).prop('checked', true);
    if($('input[name=question5]:checked').val() == 3){
        $('.q5bchat').removeClass('hide');
        $('.specialBtn').addClass('disabledbutton');
    }
    else{
        $('.q5bchat').addClass('hide');
        $('.specialBtn').removeClass('disabledbutton');
    }
});



$('.NewArgBtn').click(function(){
	$('.MsgText2').val('');
	$('#myModal1').modal();
});

$('.NewArgBtn2').click(function(){
	$('.MsgText3').val('');
	$('#myModal2').modal();
});

$('.NewArgBtn3').click(function(){
	$('.MsgText4').val('');
	$('#myModal3').modal();
});

$('.NewArgBtn4').click(function(){
	$('.MsgText5').val('');
	$('#myModal4').modal();
});

$('.AddMsgBtn1').click(function(){
	var reasoningList = $('.rlist1');
	var text = $('.MsgText2').val();
	if(text.trim()) {
        reasoningList.append('<li class="answer list-group-item list-group-item-primary active" data-id="-1" style="cursor: pointer;"><span class="state-icon glyphicon glyphicon-check"></span>' + text + '</li>');
        $('.NewArgBtn').addClass('hide');
        $('.NewArgBtn2').addClass('hide');
        $('.NewArgBtn3').addClass('hide');
        $('.NewArgBtn4').addClass('hide');
        f();
    }
	$('#myModal1').modal('toggle');
    $('.specialBtn').removeClass('disabledbutton');
});
$('.AddMsgBtn5').click(function(){
    $('#myModal5').modal('toggle');
});
$('.AddMsgBtn2').click(function(){
	var reasoningList = $('.rlist2');
	var text = $('.MsgText3').val();
    if(text.trim())  {
        reasoningList.append('<li class="answer list-group-item list-group-item-primary active" data-id="-1" style="cursor: pointer;"><span class="state-icon glyphicon glyphicon-check"></span>' + text + '</li>');
        $('.NewArgBtn').addClass('hide');
        $('.NewArgBtn2').addClass('hide');
        $('.NewArgBtn3').addClass('hide');
        $('.NewArgBtn4').addClass('hide');
        f();
    }
	$('#myModal2').modal('toggle');
    $('.specialBtn').removeClass('disabledbutton');
});

$('.AddMsgBtn3').click(function(){
	var reasoningList = $('.rlist3');
	var text = $('.MsgText4').val();
    if(text.trim()) {
        reasoningList.append('<li class="answer list-group-item list-group-item-primary active" data-id="-1" style="cursor: pointer;"><span class="state-icon glyphicon glyphicon-check"></span>' + text + '</li>');
        $('.NewArgBtn').addClass('hide');
        $('.NewArgBtn2').addClass('hide');
        $('.NewArgBtn3').addClass('hide');
        $('.NewArgBtn4').addClass('hide');
        f();
    }
	$('#myModal3').modal('toggle');
    $('.specialBtn').removeClass('disabledbutton');
});

$('.AddMsgBtn4').click(function(){
	var reasoningList = $('.rlist4');
	var text = $('.MsgText5').val();
    if(text.trim())  {
        reasoningList.append('<li class="answer list-group-item list-group-item-primary active" data-id="-1" style="cursor: pointer;"><span class="state-icon glyphicon glyphicon-check"></span>' + text + '</li>');
        $('.NewArgBtn').addClass('hide');
        $('.NewArgBtn2').addClass('hide');
        $('.NewArgBtn3').addClass('hide');
        $('.NewArgBtn4').addClass('hide');
        f();
    }
	$('#myModal4').modal('toggle');

    $('.specialBtn').removeClass('disabledbutton');
});

var f = function(){

	$('.list-group.checked-list-box .list-group-item').each(function () {
		// Settings
		var $widget = $(this),
			$checkbox = $('<input type="checkbox" class="hidden" checked/>'),
			color = ($widget.data('color') ? $widget.data('color') : 'primary'),
			style = ($widget.data('style') == 'button' ? 'btn-' : 'list-group-item-'),
			settings = {
				on: {
					icon: 'glyphicon glyphicon-check'
				},
				off: {
					icon: 'glyphicon glyphicon-unchecked'
				}
			};

		$widget.css('cursor', 'pointer');
		if($widget.hasClass('active'))
			$widget.append($checkbox);
		else{
			$checkbox =$('<input type="checkbox" class="hidden"/>');
			$widget.append();
		}

		updateDisplay();

		// Event Handlers
		$widget.on('click', function () {
			$checkbox.prop('checked', !$checkbox.is(':checked'));
			$checkbox.triggerHandler('change');
			updateDisplay();
		});
		$checkbox.on('change', function () {
			updateDisplay();
		});


		// Actions
		function updateDisplay() {
			var isChecked = $checkbox.is(':checked');

			// Set the button's state
			$widget.data('state', (isChecked) ? 'on' : 'off');

			// Set the button's icon
			$widget.find('.state-icon')
				.removeClass()
				.addClass('state-icon ' + settings[$widget.data('state')].icon);

			// Update the button's color
			if (isChecked) {
				$widget.addClass(style + color + ' active');
                $('.specialBtn').removeClass('disabledbutton');

            } else {
				$widget.removeClass(style + color + ' active');
				if($('.list-group-item-primary').hasClass('active')){
                    $('.specialBtn').removeClass('disabledbutton');
				}
				else{
                    $('.specialBtn').addClass('disabledbutton');
				}
			}
		}

		// Initialization
		function init() {

			if ($widget.data('checked') == true) {
				$checkbox.prop('checked', !$checkbox.is(':checked'));
			}

			updateDisplay();

			// Inject the icon if applicable
			if ($widget.find('.state-icon').length == 0) {
				$widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
			}
		}
		init();
	});

	$('#get-checked-data').on('click', function(event) {
		event.preventDefault();
		var checkedItems = {}, counter = 0;
		$('#check-list-box li.active').each(function(idx, li) {
			checkedItems[counter] = $(li).text();
			counter++;
		});
		$('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
	});

};

$(f);

$.fn.commentCards = function(){

	return this.each(function(){

		var $this = $(this),
			$cards = $this.find('.card'),
			$current = $cards.filter('.card--current'),
			$next;
		$cards.removeClass('card--current card--out card--next');
		$current.addClass('card--out');
		$next = $current.next();
		$next = $next.length ? $next : $cards.first();
		$next.addClass('card--current');

		/* $cards.on('click',function(){
             if ( !$current.is(this) ) {
                 $cards.removeClass('card--current card--out card--next');
                 $current.addClass('card--out');
                 $current = $(this).last().addClass('card--current');
                 $next = $current.next();
                 $next = $next.length ? $next : $cards.first();
                 $next.addClass('card--next');
             }
         });*/

		/*if ( !$current.length ) {
             $current = $cards.last();
             $cards.first().trigger('click');
         }*/

		$this.addClass('cards--active');

	});

};
$('.cards').commentCards();



