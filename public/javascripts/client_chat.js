//Variables iniciales
//var serverURL = "http://localhost:3000";
var serverURL = "http://localhost:3000";
//var serverURL = "http://192.168.50.22:3000"
//var jaimeURL = "http://192.168.1.139:3001";
var server = io.connect(serverURL);
var username = "";
var userID = -1;
var timer = 0;
var TimeConsenso = 1000*60*(2); //(min) para empezar a argumentar

//Hay 5 etapas claras en el Chat:
//  inicio: Esperando el voto del cliente
//  primeraEtapa: Esperando el voto de los otros integrantes
//  segundaEtapa: Esperando el fin del Timer para buscar un concenso
//  terceraEtapa: Esperando el fin del Timer para votar por segunda vez
//  cuartaEtapa:  Esperando el ultimo voto del cliente
//  quintaEtapa:  Esperando que todos los usuarios terminen
var msgInicio = "Hola, bienvenido, porfavor lee el tema y vota"
var msgEtapa1 = "Falta que voten los otro integrantes :)"
var msgEtapa2 = "Argumenta sobre que opcion es mejor con los otros integrantes :)"
var msgEtapa3 = "Trata de llegar a un consenso con los otros integrantes :)"
var msgEtapa4 = "Debes votar por la opcion que prefieras :)"
var msgEtapa5 = "Gracias por terminar este topico, ahora espera a que los demas voten"

//Evento de conexion que maneja el flujo de donde estoy en el proceso del chat
server.on('connect', function(socket){

    username = get_sent_data()['nombre_s'];
    server.emit('connectedToChat', username, getLocalStorage("TopicoActual"));
    //Flujo del Chat
    if(!checkLocalStorage("username"))
    {
        setLocalStorage("username", username, 1);
        setLocalStorage("inicio","True",1);

        //document.getElementById("panelDerecho").style.display = 'block';
        document.getElementsByClassName("input-group")[0].style.display = 'none'
        addBtnsFunction(primerVoto);
    }else{
        if(!checkLocalStorage("primeraEtapa"))
        {
            document.getElementsByClassName("input-group")[0].style.display = 'none'
            addBtnsFunction(primerVoto);
        }else{
            if(!checkLocalStorage("segundaEtapa"))
            {
                document.getElementsByClassName("input-group")[0].style.display = 'none'
                switchBtns();
                SendMsg(msgEtapa1,"admin", username);
            }else{
                if(!checkLocalStorage("terceraEtapa"))
                {
                    SendMsg(msgEtapa2,"admin", username);
                    switchBtns();
                }else{
                    if(!checkLocalStorage("cuartaEtapa"))
                    {
                        SendMsg(msgEtapa3, "admin", username);
                        switchBtns();
                    }else{
                        //Solo esconder el input del chat, no todo el chat
                        SendMsg(msgEtapa4, "admin", username);
                        document.getElementsByClassName("input-group")[0].style.display = 'none'
                        addBtnsFunction(segundoVoto);
                    }
                }
            }
        }
    }
});

//Ocurre cada vez que haya una conexion estando los tres conectados(refresh)
//ChatTime es el tiempo a mostrar en el timer, desde donde deberia restarse
server.on("FullVoto1", function(ChatTime){
    setLocalStorage("segundaEtapa","True",1);
    var Targ = ChatTime - TimeConsenso;
    setTimeout(AhoraArgumentaremos,Targ);

    //document.getElementById("panelDerecho").style.display = 'block';
    document.getElementsByClassName("input-group")[0].style.display = 'block'
    SendMsg(msgEtapa2, "admin", username);

    timerCasero(ChatTime);
});

//EDITAR <-------------------------------------------------------------------------------------------
server.on("FullVoto2", function(ChatTime){
    if(getLocalStorage("TopicoSiguiente")!=-1){

        setLocalStorage("TopicoActual", getLocalStorage("TopicoSiguiente"), 1);
        setLocalStorage("TopicoCont", getLocalStorage("TopicoCont")+1, 1);

        removeLocalStorage("primerVoto","",-1);
        removeLocalStorage("primeraEtapa","",-1);
        removeLocalStorage("segundaEtapa","",-1);
        removeLocalStorage("terceraEtapa","",-1);
        removeLocalStorage("cuartaEtapa","",-1);
        removeLocalStorage("TopicoSiguiente","",-1);

        location.reload();
    }else{
        //IP JAIME
        window.location=serverURL;
    }
});

//Carga los argumentos iniciales desde la DB
//Ademas nos indicara cuando un Chat debe seguir siendo chat o cuando debe
//pasar a la app de argumentacion
//Las cookies declaradas aca seran usadas en el voto2, para ver si seguir o
//pasar a la app argumentativa
server.on("LoadArgs", function(argArray, name){
    if(name == username)
    {
        if(!checkLocalStorage("Topicos"))
        {
            setLocalStorage("Topicos",argArray[0]);
            setLocalStorage("TopicoActual", argArray[0][0],1);
            setLocalStorage("TopicoCont",0,1);
        }

        var topicosStr = getLocalStorage("Topicos");
        var topicos = topicosStr.split(",");
        var topicoCont = parseInt(getLocalStorage("TopicoCont"));

        if(!checkLocalStorage("TopicoSiguiente"))
        {
            if(topicos.length - 1 <= topicoCont)
            {
                setLocalStorage("TopicoSiguiente", -1, 1);
            }else{
                setLocalStorage("TopicoSiguiente", topicos[topicoCont + 1],1);
            }
        }
        console.log("HEY",argArray,topicoCont)
        for(var i = 0; i < argArray[3][topicoCont].length; i++)
        {
            var btn = document.getElementById("b"+(i+1));
            btn.innerHTML = argArray[3][topicoCont][i].text;
            btn.setAttribute("argid", ""+argArray[3][topicoCont][i].id);
        }
        frontendCards(argArray[1][topicoCont].text);
        server.emit('CargarMensajesChat', username, getLocalStorage("TopicoActual"));
    }
});

server.on("MsgReceived", function(data){
    if(data.sentTo == "all")
    {
        displayMsgReceived(data);
    }else{
        if(data.sentTo == "allButMe")
        {
            if(data.from != username){
                displayMsgReceived(data);
            }
        }else{
            if(data.sentTo == username){
                displayMsgReceived(data);
            }
        }
    }
});

//Debe avisar cuando se conecte un usuario
server.on("UserConnectedToChat", function(name){
    if(name == username)
    {
        SendMsg("Se conecto "+name, "admin", "allButMe");
    }
});

server.on("CargarLogs", function(LogArray){
    console.log(LogArray);
});

function displayMsgReceived(data)
{
    var t = new Date(),
        curHour = t.getHours() > 12 ? t.getHours() - 12 : (t.getHours() < 10 ? "0" + t.getHours() : t.getHours()),
        curMinute = t.getMinutes() < 10 ? "0" + t.getMinutes() : t.getMinutes(),
        curSeconds = t.getSeconds() < 10 ? "0" + t.getSeconds() : t.getSeconds();
    var time = "Enviado a las "+formatAMPM(t);

    var cl = $("#chat");
    var div  = cl.parent();

    if(data.typeOfUser == "admin")
    {
        //Algo especial x ser un mensaje de admin
        cl.append("<li class='clearfix'><div class='bubble clearfix'><p>" + data.msge + "</p> </div> </li>");
    }else{
        if(data.from==username) {
            cl.append("<li class='right clearfix'><div class='chat-body clearfix'><div class='header-msg'><strong class='primary-font'>" + data.from + "</strong><small class='pull-right text-muted'>" + time + "</small></div><p>" + data.msge + "</p> </div> </li>");
        } else{
            cl.append("<li class='left clearfix'><div class='chat-body clearfix'><div class='header-msg'><strong class='primary-font'>" + data.from + "</strong><small class='pull-right text-muted'>" + time + "</small></div><p>" + data.msge + "</p> </div> </li>");
        }
    }

    div.animate({ scrollTop: div.prop("scrollHeight")}, 300);
}

function SendMsg(strMsg, typeOfUser, sentTo)
{
    var arg_topic_id =  parseInt(getLocalStorage("TopicoActual"));
    var message = {
        from: username,
        msge: strMsg,
        typeOfUser: typeOfUser,
        sentTo: sentTo,
        arg_topic_id: arg_topic_id
    };

    if(strMsg!=""){
        server.emit("MsgSent",message);
        console.log("Mensaje: ");
        console.log(message);
    }
}

function timerCasero(ChatTime)
{
    //Timer casero
    timer  = ChatTime;
    //var bigDiv = document.getElementsByClassName('panelIzquierdo')[0];
    //bigDiv.appendChild(g);
    $('#timer').text("Tiempo Restante: "+ChatTime);
    updateTimer();
}

function primerVoto(){
    setLocalStorage("primeraEtapa", "True",1);
    var argId = parseInt(this.getAttribute("ArgID"));
    var topicID = parseInt(getLocalStorage("TopicoActual"));
    var voto = { from: username, arg_topic_id: topicID, arg_id: argId};
    var bId = this.id;
    SendMsg(msgEtapa1, "admin", username);
    server.emit('primerVoto', voto);
    switchBtns();
    $("#"+bId).removeClass('disabledbutton');
    removeBtnsFunction(primerVoto);
}

function segundoVoto(){
    var argId = parseInt(this.getAttribute("ArgID"));
    var topicID = parseInt(getLocalStorage("TopicoActual"));
    var voto = { from: username, arg_topic_id: topicID, arg_id: argId};
    var bId = this.id;
    setLocalStorage("segundoVoto", "True",1);
    server.emit('segundoVoto', voto);
    switchBtns();
    $("#"+bId).removeClass('disabledbutton');
    SendMsg(msgEtapa5, "admin", username);
}

//definir como mostrar dicho mensaje
function AhoraArgumentaremos(){
    SendMsg(msgEtapa3, "admin", username);
    setLocalStorage("terceraEtapa","True",1);
    setTimeout(AhoraSegundoVoto, TimeConsenso);
}

function AhoraSegundoVoto(){
    SendMsg(msgEtapa4, "admin", username);
    setLocalStorage("cuartaEtapa","True",1);
    addBtnsFunction(segundoVoto);
    switchBtns();
    document.getElementsByClassName("input-group")[0].style.display = 'none'
    //document.getElementById("panelDerecho").style.display = 'none';
}

function get_sent_data()
{
    // funcion creada para obtener la informacion en get
    var loc = document.location.href;
    // si existe el interrogante
    if(loc.indexOf('?')>0)
    {
        // cogemos la parte de la url que hay despues del interrogante
        var getString = loc.split('?')[1];
        // obtenemos un array con cada clave=valor
        var GET = getString.split('&');
        var get = {};
        // recorremos todo el array de valores
        for(var i = 0, l = GET.length; i < l; i++){
            var tmp = GET[i].split('=');
            get[tmp[0]] = unescape(decodeURI(tmp[1]));
        }
        return get;
    }
}

function formatAMPM(date)
{
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function addBtnsFunction(funcion)
{
    for(var i = 0; i < 3; i++)
    {
        var btn = document.getElementById("b"+(i+1));
        btn.addEventListener("click", funcion);
    }
}

function switchBtns()
{
    for(var i = 0; i < 3; i++)
    {
        var btn = document.getElementById("b"+(i+1));
        btn.disabled = !btn.disabled;
        if(btn.disabled){
            $('.answer').addClass('disabledbutton');
            // document.getElementById("b"+(i+1)).style.background='#123000';
        }else{
            $('.answer').removeClass('disabledbutton');
        }
    }
}

function removeBtnsFunction(funcion)
{
    for(var i = 0; i < 3; i++)
    {
        var btn = document.getElementById("b"+(i+1));
        btn.removeEventListener("click", funcion);
    }
}

function frontendCards(text)
{
    //CARDS
    var cl = $('.cards');
    $('#description').text(text.split('#')[0]);
    cl.append("<li class='card card-short'><div class='commentText comment'><p><strong>"+text.split('#')[text.split('#').length-1]+"</strong></p></div></li>");
    $('.cards').each(function(){
        var $this = $(this),
            $cards = $this.find('.card'),
            $current = $cards.filter('.card--current'),
            $next;
        $cards.removeClass('card--current card--out card--next');
        $current.addClass('card--out');
        $next = $current.next();
        $next = $next.length ? $next : $cards.first();
        $next.addClass('card--current');
        $this.addClass('cards--active');
    })
}

//Local storage va a tener cname++++user : value
function setLocalStorage(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    var cname2 =  cname+"_____user"+username;
    localStorage.setItem(cname2,cvalue)
    // + ";" + expires + ";path=/chat?nombre_s="+username+"&status=new";
    //document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/chat?nombre_s="+username+"&status=new";
    console.log("Storage add --> "+ cname2 + ": " + cvalue);
}

function removeLocalStorage(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    var cname2 =  cname+"_____user"+username;
    localStorage.removeItem(cname2)
    // + ";" + expires + ";path=/chat?nombre_s="+username+"&status=new";
    //document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/chat?nombre_s="+username+"&status=new";
    console.log("Storage remove --> "+ cname2 + ": " + cvalue);
}

function getLocalStorage(cname) {
    //var name = cname + "=";
    //var decodedCookie = decodeURIComponent(document.cookie);
    //var ca = decodedCookie.split(';');
    var cname2 = cname+"_____user"+username;
    var name = "";
    console.log("buscare en localStorage a :"+cname2);
    if(localStorage.getItem(cname2))
    {
        return localStorage.getItem(cname2);
    }else
    {
        return "";
    }

}

function checkLocalStorage(cname) {
    var cname_ = getLocalStorage(cname);
    if (cname_ != "") {
        //alert("Hola, " + cname + " es " +cname_);
        return true;
    } else {
        return false;
    }
}

function updateTimer()
{
    var t = timer
    var t_new = t - 50;
    $('#timer').text( getDuration(t_new));
    timer = t_new;
    var gua = setTimeout(updateTimer, 50);
}

function getDuration(time)
{
    console.log(""+Math.floor(time/1000/60) - Math.floor((Math.floor(time/1000/60)))*60);

    var decimal = ((time/1000/60.0 - Math.floor(time/1000/60.0))*60);
    if(time<0){
        return "";
    }
    if(decimal<10){
        decimal = "0"+Math.floor(decimal);
    }
    else{
        decimal = Math.floor(decimal);
    }

    return "Tiempo restante: "+Math.floor(time/1000/60)+":"+decimal;

}

// Again, like game.js, why doesn't this do anything in each case?
$("#SendMsgBtn").click(function(){

    var strMsg = $("#MsgText").val();
    $("#MsgText").val("");
    var message = {
        from: username,
        msge: strMsg,
        typeOfUser: "user",
        sentTo: "all"
    };
    if(strMsg!=""){
        server.emit("MsgSent",message);
    }
});

$("#MsgText").on("keypress", function(e) {
    /* ENTER PRESSED*/
    if (e.keyCode == 13) {
        var strMsg = $("#MsgText").val();
        $("#MsgText").val("");
        var message = {
            from: username,
            msge: strMsg,
            typeOfUser: "user",
            sentTo: "all"
        };
        if(strMsg!=""){
            server.emit("MsgSent",message);
        }

    }
});

$('.SendMsgBtn1').click(function(){
    var ans = $('input[name=question2]:checked').val();
});

$('.SendMsgBtn2').click(function(){
    if($('input[name=question2]:checked').val() == 0){
        $('.q2achat').hide();
        $('.q2bchat').removeClass('hide');
    }
});

$('.SendMsgBtn3').click(function(){
});

$('.SendMsgBtn4').click(function(){
});

$('.NewArgBtn').click(function(){
    $('#myModal').modal();
});

$(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {

        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });


        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {

            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }

            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });

    $('#get-checked-data').on('click', function(event) {
        event.preventDefault();
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});

