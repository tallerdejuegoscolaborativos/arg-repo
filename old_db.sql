-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: DB_2gr_1ses_6al_2pr
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Arguments`
--

DROP TABLE IF EXISTS `Arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Arguments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `text` text NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `image_name` text NOT NULL,
  `topic_id` int(11) NOT NULL,
  `topic_text` text NOT NULL,
  `depth` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Arguments`
--

LOCK TABLES `Arguments` WRITE;
/*!40000 ALTER TABLE `Arguments` DISABLE KEYS */;
INSERT INTO `Arguments` VALUES (0,0,NULL,'Quien ganaria entre KingKong y GodZilla?',NULL,1,1,'godzilla',0,'Quien ganaria entre KingKong y GodZilla?',0),(1,1,0,'Ganaría KingKong por ser más fuerte',NULL,1,1,'godzilla',0,'Quien ganaria entre KingKong y GodZilla?',1),(2,1,0,'Ganaría GodZilla por ser más rápido',NULL,1,1,'godzilla',0,'Quien ganaria entre KingKong y GodZilla?',1),(3,1,0,'Empatarían porque son igual de poderosos',NULL,1,1,'godzilla',0,'Quien ganaria entre KingKong y GodZilla?',1);
INSERT INTO `Arguments` VALUES (4,0,NULL,'Se le debe entregar mar a Bolivia?',NULL,2,1,'mar',4,'Se le debe entregar mar a Bolivia?',0),(5,1,0,'No se le debería entregar mar porque no existe una obligación legal',NULL,2,1,'mar',4,'Se le debe entregar mar a Bolivia?',1),(6,1,0,'Se le debería entregar mar porque existe una deuda histórica',NULL,2,1,'mar',4,'Se le debe entregar mar a Bolivia?',1),(7,1,0,'Se debería buscar otro arreglo que no involucre mar o territorio',NULL,2,1,'mar',4,'Se le debe entregar mar a Bolivia?',1);
INSERT INTO `Arguments` VALUES (8,0,NULL,'Quien ganaria entre KingKong y GodZilla?',NULL,2,1,'godzilla',8,'Quien ganaria entre KingKong y GodZilla?',0),(9,1,0,'Ganaría KingKong por ser más fuerte',NULL,2,1,'godzilla',8,'Quien ganaria entre KingKong y GodZilla?',1),(10,1,0,'Ganaría GodZilla por ser más rápido',NULL,2,1,'godzilla',8,'Quien ganaria entre KingKong y GodZilla?',1),(11,1,0,'Empatarían porque son igual de poderosos',NULL,2,1,'godzilla',8,'Quien ganaria entre KingKong y GodZilla?',1);
INSERT INTO `Arguments` VALUES (12,0,NULL,'Se le debe entregar mar a Bolivia?',NULL,1,1,'mar',12,'Se le debe entregar mar a Bolivia?',0),(13,1,0,'No se le debería entregar mar porque no existe una obligación legal',NULL,1,1,'mar',12,'Se le debe entregar mar a Bolivia?',1),(14,1,0,'Se le debería entregar mar porque existe una deuda histórica',NULL,1,1,'mar',12,'Se le debe entregar mar a Bolivia?',1),(15,1,0,'Se debería buscar otro arreglo que no involucre mar o territorio',NULL,1,1,'mar',12,'Se le debe entregar mar a Bolivia?',1);

/*!40000 ALTER TABLE `Arguments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socket` text,
  `session_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,NULL,1),(2,NULL,1);
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Logs`
--

DROP TABLE IF EXISTS `Logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session` int(11) NOT NULL,
  `message` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Logs`
--

LOCK TABLES `Logs` WRITE;
/*!40000 ALTER TABLE `Logs` DISABLE KEYS */;
INSERT INTO `Logs` VALUES (1,1,1,' qu 1 ','2017-08-03 16:19:42'),(2,2,1,' qu 1 ','2017-08-03 16:22:34'),(3,2,1,'Type 1 response received from user 2 on arg 1 from group 0','2017-08-03 16:22:37'),(4,2,1,' qu type 2','2017-08-03 16:22:37'),(5,2,1,'Type 2 response received from user 2 on arg 1 from group 0','2017-08-03 16:22:48'),(6,2,1,' qu type 1','2017-08-03 16:22:48'),(7,1,1,' qu 1 ','2017-08-03 19:48:27'),(8,54,17,' qu 1 ','2017-09-08 01:52:20'),(9,54,17,' qu 1 ','2017-09-08 05:29:43'),(10,81,1,' qu 1 ','2018-03-20 13:27:41');
/*!40000 ALTER TABLE `Logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Question_Children`
--

DROP TABLE IF EXISTS `Question_Children`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Question_Children` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Question_Children`
--

LOCK TABLES `Question_Children` WRITE;
/*!40000 ALTER TABLE `Question_Children` DISABLE KEYS */;
/*!40000 ALTER TABLE `Question_Children` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Questions`
--

DROP TABLE IF EXISTS `Questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Questions`
--

LOCK TABLES `Questions` WRITE;
/*!40000 ALTER TABLE `Questions` DISABLE KEYS */;
INSERT INTO `Questions` VALUES (1,1,81,1,'2018-03-20 13:27:41');
/*!40000 ALTER TABLE `Questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Chatlogs`
--

DROP TABLE IF EXISTS `Chatlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chatlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Chatlogs`
--

LOCK TABLES `Chatlogs` WRITE;
/*!40000 ALTER TABLE `Chatlogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chatlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Response_Arguments`
--

DROP TABLE IF EXISTS `Response_Arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Response_Arguments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `argument_text` text NOT NULL,
  `vote` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `Responses`
--

DROP TABLE IF EXISTS `Responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Responses`
--

LOCK TABLES `Responses` WRITE;
/*!40000 ALTER TABLE `Responses` DISABLE KEYS */;
/*!40000 ALTER TABLE `Responses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sessions`
--

DROP TABLE IF EXISTS `Sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sessions`
--

LOCK TABLES `Sessions` WRITE;
/*!40000 ALTER TABLE `Sessions` DISABLE KEYS */;
INSERT INTO `Sessions` VALUES (1,'test');
/*!40000 ALTER TABLE `Sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Franco_1',1),(2,'Alberto_1',1),(3,'Jaime_1',1),(4,'Franco_2',2),(5,'Alberto_2',2),(6,'Jaime_2',2);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Votes`
--

DROP TABLE IF EXISTS `Votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `vote` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Votes`
--

LOCK TABLES `Votes` WRITE;
/*!40000 ALTER TABLE `Votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `Votes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-11 18:08:35