module.exports= function(server, connection, session_id){

	require('./database.js')(connection,session_id);


	const data=new Data();

	require('./helper_functions.js')(connection,session_id, data);

	const help=new Help();

	const MAX_DEPTH=5

	let args=data.loadArgsArray();
	let users=[];
	let votes=[];
	let questionQueue=[];
	let usersWaitingForFreeArg=[];

	const io = require('socket.io').listen(server);

	io.sockets.on('connection', function (socket) {

		console.log('Server ready for events');

        socket.on('connectedToChat',function(name, topicId)
        {
            //Se haran las conexiones al grupo
            //console.log("User "+ name+ " connected");
            if (isNewUser(name)){	addUser(name);	}
            var user = getUser(name);

            //Unimos al usuario a la conversacion
            var grupo = user.groupId;
            socket.join(""+grupo);

            //Carga los argumentos del grupo en el cliente del usuario
            var argGrupo = data.loadArgGrupo(grupo);
            console.log("ARGGRUPO", argGrupo)

            var DataLoadArgs = ArgGrupoToLoadArgs(argGrupo);
            console.log("DATALOADARGS", DataLoadArgs)

            io.sockets.in(""+grupo).emit("LoadArgs",DataLoadArgs, name);

            //Avisa al resto del grupo que el usuario se conecto.
            io.sockets.in(""+grupo).emit("UserConnectedToChat", name);
            SetTimerGroup(grupo, topicId);

        });

        socket.on('primerVoto',function(voto){
            user = getUser(voto.from);
            var VotoDBaux = {userId: user.id, groupId: user.groupId, topicId: voto.arg_topic_id, arg_id: voto.arg_id, numero: 1};
            data.ChatSubmitVote(VotoDBaux);
            var groupID = user.groupId;
            var topicId = voto.arg_topic_id;
            //console.log("En primer Voto el topic id es" +topicId.toString());
            SetTimerGroup(groupID, topicId);
        });

        socket.on('segundoVoto',function(voto){
            user = getUser(voto.from);
            var VotoDBaux = {userId: user.id, groupId: user.groupId, topicId: voto.arg_topic_id, arg_id: voto.arg_id, numero: 2};
            data.ChatSubmitVote(VotoDBaux);
            var groupID = user.groupId;
            var topicId = voto.arg_topic_id;
            UnSetTimerGroup(groupID, topicId);
        });

        //Xq tiene que validar al user? no solo usarlo
        socket.on('MsgSent', function(msg)
        {
            if(isNewUser(msg.from))
            {
                addUser(msg.from);
            }
            user = getUser(msg.from);

            io.sockets.in(""+user.groupId).emit("MsgReceived",msg);
            var ChatAuxLog =  {userId: user.id, groupId: user.groupId, text: msg.msge, typeOfUser: msg.typeOfUser, arg_topic_id: msg.arg_topic_id};
            data.MsgSentLog(ChatAuxLog);
        });

        socket.on('CargarMensajesChat', function(username, topicId)
        {
            console.log(username);
            console.log(topicId);
            if(isNewUser(username))
            {
                addUser(username);
            }
            user = getUser(username);
            var groupId = user.groupId;
            var Logs = data.loadChatLogs(groupId, topicId);
            io.sockets.in(""+user.groupId).emit("CargarLogs",Logs);
        });

        function SetTimerGroup(groupID, topicId)
        {
            if(!(topicId===""))
            {
                //console.log("En SetTimerGroup topicId es :"+topicId.toString()+"\n");
                if(data.CheckFullVoto(groupID, 1, topicId))
                {
                    //var grupoIds = data.FullGroupId(groupID);
                    var time = new Date().getTime();
                    var startTime = data.setTimeFullGroup(groupID, time, false);
                    console.log("TIME: "+time+ "\t startTime: "+startTime);
                    var ChatTime = ChatDuration - (time - startTime);
                    console.log("TIMECHAT: "+ChatTime);
                    io.sockets.in(""+groupID).emit("FullVoto1",ChatTime);
                }
            }

        }

        function UnSetTimerGroup(groupID, topicId)
        {
            if(data.CheckFullVoto(groupID, 2, topicId))
            {
                //var grupoIds = data.FullGroupId(groupID);
                var startTime = data.setTimeFullGroup(groupID, 0, true);
                io.sockets.in(""+groupID).emit("FullVoto2", startTime);
            }
        }

        //En desuso
        //Inicia los timers enviando el evento StartTimer
        function fullGrupo(groupID)
        {
            //argIniciales ya no va, deben estar cargados, solo hacerlos clickeables en funcion
            //de current Time vs timefullGroup

            var grupoIds = data.FullGroupId(groupID);
            var time = new Date().getTime();
            var startTime = data.setTimeFullGroup(groupID, time, false);
            console.log("TIME: "+time+ "\t startTime: "+startTime);
            var ChatTime = ChatDuration - (time - startTime);
            io.sockets.in(""+groupID).emit("StartTimer", ChatTime);
            //io.sockets.in(""+user.groupId).emit("StartTimer", timeChat, argIniciales, timeFullgruop);
            console.log("todos los usuarios del grupo "+groupID+" estan conectados, ChatTime: "+ ChatTime);
        }

        function isGroupInUsers(grupo, usersArray)
        {
            //console.log("GroupID "+ grupo);
            var grupoIds = data.FullGroupId(grupo);
            var usersId = Array();
            for (i = 0; i < usersArray.length; i++) {
                usersId.push(usersArray[i].id);
            }
            var Full = grupoIds.every(function(val) {return usersId.indexOf(val) >= 0; });
            return Full;
        }

        //Retorna 4 arreglos con toda la informacion nesecaria de los argumentos
        function ArgGrupoToLoadArgs(argGrupo)
        {
            var topicosID = [];
            var argInicialesID = [];
            var argTopicosID = [];
            var argIniciales = [];
            var argTopicos = [];
            for(var i = 0; i < argGrupo.length; i++)
            {
                if(topicosID.indexOf(argGrupo[i].topicId)== -1)
                {
                    topicosID.push(argGrupo[i].topicId);
                    argTopicosID.push(argGrupo[i].id);
                }
            }

            for(var j = 0; j < topicosID.length; j++)
            {
                var auxArg = [];
                for(var i = 0; i < argGrupo.length; i++)
                {
                    if(argGrupo[i].topicId == topicosID[j])
                    {
                        if(argGrupo[i].type == 1)
                        {
                            auxArg.push(argGrupo[i]);
                            argInicialesID.push(argGrupo[i].id);
                        }else if(argGrupo[i].type == 0){
                            argTopicos.push(argGrupo[i]);
                        }else{
                            var nolauso = 0;
                        }
                    }
                }
                argIniciales.push(auxArg);
            }
            var retorno = [];
            retorno.push(argTopicosID);
            retorno.push(argTopicos);
            retorno.push(argInicialesID);
            retorno.push(argIniciales);
            return retorno;
        }


        function isNewUser(name)
        {
            return !Array.from(users,function(u){return u.name;}).includes(name);
        }

        function getUser(name)
        {
            return users.find(function(u){return u.name===name});
        }

        function addUser(name)
        {
            user = data.loadUser(name, socket, connection);
            users.push(user);
        }










		socket.on('userConnected', async function(name){


			console.log('User '+ name+ ' connected');


			let user;
			let isNewUser = await !Array.from(users,function(u){return u.name;}).includes(name);


			if (isNewUser){
				user= await data.loadUser(name, socket, connection);
				users.push(user);


				usersWaitingForFreeArg[user.groupId]=[];


			} else {
				user=await users.find(function(u){return u.name===name;});

			}

			//Por ahora manda tipo 1 al comienzo, después se debe introducir manejo de desconexiones

			let argForQuestion=await help.findFreeArg(args, user, votes, null); //false if no free arg


			if (argForQuestion){
				argForQuestion.busy=true;


				let question= await data.createQuestion({type:1, user:user, arg:argForQuestion, argsArray:args, votesArray:votes, imageName:argForQuestion.imageName, topicId:argForQuestion.topicId});

				user.currentTopic=argForQuestion.topicId;

				io.to(user.socketId).emit('Qu', question);
				console.log('Server sent ' + user.name + ' question type ' + 1);

			} else {
				console.log(user.name + ' has no free arguments');
			}



		});


		socket.on('clientResponse', async function(resp) {


			let user=await users.find(function(u){return u.id===resp.userID;});
			await data.saveResponse(resp);

			let arrayQuestionsInQueueForUser=await questionQueue.filter(function(q){return q.userID===user.id;});

			let question;

			//if there is a question in queue send that
			if (arrayQuestionsInQueueForUser.length!==0){

				question= arrayQuestionsInQueueForUser.pop();

                var index = questionQueue.indexOf(question);
                if (index > -1) {
                    questionQueue.splice(index, 1);
                }


				await io.to(user.socketId).emit('Qu', question);
				console.log('Server sent ' + user.name + ' question type ' + question.type);

			//else determine new question through algorithm
			} else {
				switch (resp.type){
					case 1:



					let vote=await data.createVote(user.id, resp.respArgID[0], resp.respArgVote[0]);
					votes.push(vote);

					let arg= await args.find(function(a){return a.id===resp.argID;});
					arg.busy=false;

					if(arg.depth<=MAX_DEPTH) {

                        arg.busy = true;

                        //returns one question if voted yes or no, two questions if voted "I don't know" (as dictated by algorithm)
                        let questionArray = await help.questionsAccordingToVote(resp.respArgVote[0], user, arg, args, votes);

                        if (questionArray.length !== 1) {

                            await io.to(user.socketId).emit('Qu', questionArray[0]);
                            questionQueue.push(questionArray[1]);

                        } else {

                            await io.to(user.socketId).emit('Qu', questionArray[0]);

                        }


                        break;

                    }

				case 2:
				case 3:

                    let prevArg= args.find(function(a){return a.id===resp.argID;});

                    prevArg.busy=false;

					//if there is new args/votes, add to database and add to args array/votes array
					if (resp.respArgVote.length!==1){

                    	let allNewArgs=await help.addNewArguments(resp.respArgID, resp.respArgText,resp.respArgVote,  resp.imageName, resp.topicId, resp.topicText, args, user);
						let newVotes=await help.addNewVotes(user, allNewArgs[0], allNewArgs[1], args);
						args=args.concat(allNewArgs[0]);
						votes=votes.concat(newVotes);


					}


					let argForQuestion=await help.findFreeArg(args, user,votes); //false if no free arg

					console.log(args)

					if (argForQuestion){

						if (argForQuestion.topicId!==user.currentTopic){

							if (await help.checkIfAllUsersVotedForTopic(user, users, votes, args, user.currentTopic)){

								let usersInSameGroup=await users.filter(x=>x.groupId===user.groupId);
								for (let otherUser of usersInSameGroup){
									let anotherArgForQuestion= await help.findFreeArg(args, otherUser,votes,null); //false if no free arg

									anotherArgForQuestion.busy=true;
									question=await data.createQuestion({type:1, user:otherUser, arg:anotherArgForQuestion, argsArray:args, votesArray:votes, imageName:anotherArgForQuestion.imageName, topicId:anotherArgForQuestion.topicId });
									await io.to(otherUser.socketId).emit('Qu', question);
									console.log('Server sent ' + otherUser.name + ' question type ' + 1);

									usersWaitingForFreeArg[user.groupId]=[];

									otherUser.currentTopic=argForQuestion.topicId;

								}


							} else {
								if (!usersWaitingForFreeArg[user.groupId].includes(user)){
                                    usersWaitingForFreeArg[user.groupId].push(user);
                                    await io.to(user.socketId).emit('wait');
								}


							}







						} else {
							argForQuestion.busy=true;
							question=await data.createQuestion({type:1, user:user, arg:argForQuestion, argsArray:args, votesArray:votes, imageName:argForQuestion.imageName, topicId:argForQuestion.topicId});
							await io.to(user.socketId).emit('Qu', question);
							console.log('Server sent ' + user.name + ' question type ' + 1);
						}




					} else {


						console.log("no hay free arg");

						let argsArray=await help.irrationalArg4(user, args, votes); //returns array of [parentArg, childArg] if irrational, false otherwise
						if (argsArray){
						    console.log("detectocontradiccion4")
							question=await help.questionIrrational4(user, args, votes, argsArray);
							await io.to(user.socketId).emit('Qu', question);
							console.log('Server sent ' + user.name + ' question type ' + 4);




						} else {



							argsArray=await help.irrationalArg5(user, args, votes); //returns array of [parentArg, childArg] if irrational, false otherwise
							if (argsArray){
                                console.log("detectocontradiccion5")
								question=await help.questionIrrational5( user, args, votes, argsArray);
								await io.to(user.socketId).emit('Qu', question);
								console.log('Server sent ' + user.name + ' question type ' + 5);




							} else {


                                user.complete=await help.userVotedForAllArgs(user,args,votes);
								let allComplete=await help.allUsersComplete(users);

								if (allComplete){

									for (user of users){
										await io.to(user.socketId).emit('complete');
									}

								} else {
                                    if (!usersWaitingForFreeArg[user.groupId].includes(user)){
                                        usersWaitingForFreeArg[user.groupId].push(user);
                                    }

                                    await io.to(user.socketId).emit('wait');
                                }
							}




						}


					}

					//if there is a waiting user that hasn't voted for the arg received, send him a type 1 question with it


					for (let waitingUser of usersWaitingForFreeArg[user.groupId]){
                        let votedForPrev=await !help.checkIfUserVotedForSpecificArg(waitingUser,prevArg,votes);
						if (prevArg.groupId===waitingUser.groupId && votedForPrev){

							prevArg.busy=true;
							question=await data.createQuestion({type:1, user:waitingUser, arg:prevArg, argsArray:args, votesArray:votes, imageName:prevArg.imageName, topicId:prevArg.topicId });
							await io.to(waitingUser.socketId).emit('Qu', question);
							console.log('Server sent ' + waitingUser.name + ' question type ' + 1);

                            let index = usersWaitingForFreeArg[user.groupId].indexOf(waitingUser);
                            if (index > -1) {
                                usersWaitingForFreeArg[user.groupId].splice(index, 1);
                            }

							break;
						}
					}

                    console.log(votes);
					console.log("WAITINGUSERS",usersWaitingForFreeArg)
					break;




				case 4:
				case 5:

					if (resp.respArgVote.length!==1){

						let newArgs=await help.addNewArguments(resp.respArgID, resp.respArgText,resp.respArgVote,  resp.imageName, resp.topicId, resp.topicText, args, user);
						let newVotes=await help.addNewVotes(user, newArgs);
						args.concat(newArgs);
						votes.concat(newVotes);
					}


					votes=await help.changeIrrationalVotes(votes, resp.respArgID,resp.childrenID[0], resp.changeIrrational);


					let argsArray=await help.irrationalArg4(user, args, votes); //returns array of [parentArg, childArg] if irrational, false otherwise

					if (argsArray){
						question=await help.questionIrrational4(user, args, votes, argsArray);
						await io.to(user.socketId).emit('Qu', question);
						console.log('Server sent ' + user.name + ' question type ' + 4);



					} else {


						argsArray=await help.irrationalArg5(user, args, votes); //returns array of [parentArg, childArg] if irrational, false otherwise

						if (argsArray){
							question=await help.questionIrrational5(user, args, votes, argsArray);
							await io.to(user.socketId).emit('Qu', question);
							console.log('Server sent ' + user.name + ' question type ' + 5);




						} else {


							user.complete=await help.userVotedForAllArgs(user,args,votes);
							let allComplete=await help.allUsersComplete(users);


							if (allComplete){
								for (user of users){
									await io.to(user.socketId).emit('complete');
								}

							} else {
                                if (!usersWaitingForFreeArg[user.groupId].includes(user)){
                                    usersWaitingForFreeArg[user.groupId].push(user);
                                }
                                await io.to(user.socketId).emit('wait');
                            }
						}




					}
					break;


				}
			}

		});

	});
};