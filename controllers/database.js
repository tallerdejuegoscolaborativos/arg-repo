module.exports= function(connection, session_id){

	function Vote({id,userId,argumentId,vote}){
		this.id=id;
		this.userId=userId;
		this.argumentId=argumentId;
		this.vote=vote;
		this.votesAlreadyRationalized=[];
	}

	function ChatLog(id,userId,argumentId, groupId, sessionId, timestamp, text){
		this.id = id;
		this.userId=userId;
		this.argumentId=argumentId;
		this.groupId = groupId;
		this.sessionId = sessionId;
		this.timestamp = timestamp;
		this.text = text;
	}

    function ChatVote(id,userId,argumentId, groupId, topicId){
        this.id = id;
        this.userId=userId;
        this.argumentId=argumentId;
        this.groupId = gruopId;
        this.topicId = topicId;
    }

	function Question({id, type, user, arg, argsArray, votesArray, imageName, topicId, allArgs}){

		this.id = id;
		this.type = type;
		this.userID=user.id;

		this.argID = arg.id;
		this.argType = arg.type;
		this.argText = arg.text;

		this.imageName=imageName;
		this.topicId=topicId;
		this.topicText=arg.topicText;


		this.childrenID = [];
		this.childrenType = [];
		this.childrenText = [];
		this.childrenVote = [];

		if (type !== 1){

			for (argI of argsArray){



				if (argI.parentID === this.argId){

					this.childrenID.push(argI.id);
					this.childrenType.push(argI.type);
					this.childrenText.push(argI.text);

					var voteInstance=votesArray.find(function(v){return v.userId===user.id && v.argId===arg.id;});
					var voteNum=voteInstance===undefined ? 0 : voteInstance.vote;
					this.childrenVote.push(voteNum);

				}
			}

		}

		this.respArgID = [];
		this.respArgText = [];
		this.respArgVote = [];

		this.changeIrrational='';

		this.ancestors=this.findAncestors(arg,allArgs);

	}

	Question.prototype.findAncestors=function (arg, args) {

		var ancestors=[];

		var currentArg=arg;

		while(true){
			ancestors.push(currentArg);

			currentArg=args.find(function(u){return currentArg.parentId===u.id;});

			if (currentArg.type===0){
				ancestors.push(currentArg);
				break;
			}
		}
		return ancestors;
	};


	function Argument(id, type, parent_id, text, owner_id, group_id, session_id, args,image_name,topic_id, topic_text, depth){

		this.id=id;
		this.type=type;
		this.parentId=parent_id;
		this.text=text;
		this.ownerId=owner_id;
		this.groupId=group_id;
		this.sessionId=session_id;
		this.busy = false;
		this.imageName=image_name;
		this.topicId=topic_id;
		this.topicText=topic_text;
		this.depth=depth;



	}

	function User(id, name, groupId, assigned_to, socket){


		this.id = id;
		this.name = name;
		this.groupId = groupId;
		this.assigned_to=assigned_to;
		this.socketId = socket.id;
		this.busy = false;
		this.complete = false;
		this.currentTopic=null;


	}

	Data=function(){

        this.ChatSubmitVote = function(voto)
        {
            var userId = voto.userId;
            var groupId = parseInt(voto.groupId);
            var topicId = parseInt(voto.topicId);
            console.log(topicId);
            var argumentId = parseInt(voto.arg_id);
            var numero = voto.numero;
            console.log('INSERT INTO ChatVotes SET user_id='+userId+', group_id='+groupId+', numero='+numero+', topic_id='+topicId+', argument_id='+argumentId);
            connection.query('INSERT INTO ChatVotes SET user_id='+userId+', group_id='+groupId+', numero='+numero+', topic_id='+topicId+', argument_id='+argumentId);
        };

        this.CheckFullVoto= function(grupo, numero, topicId)
        {
            console.log('SELECT * FROM ChatVotes WHERE ( group_id = ' + grupo + ' AND numero = '+numero+' AND topic_id = '+ topicId+ ')')
            var results=connection.query('SELECT * FROM ChatVotes WHERE ( group_id = ' + grupo + ' AND numero = '+numero+' AND topic_id = '+ topicId+ ')');
            console.log(results);
            return (results.length == 3);
        }


		this.loadArgsArray=function(){

			argArray=Array();

			var results=connection.query('SELECT * FROM Arguments WHERE session_id = ' + session_id + ' ORDER BY id');

			var argId, argType, argParentId, argText, argOwnerId, argGroupId, argSessionId, argImageName, argTopicId, argTopicText, argDepth;

			for (var i=0; i<results.length; i++){


				argId=results[i].id;
				argType=results[i].type;
				argParentId=results[i].parent_id;
				argText=results[i].text;
				argOwnerId=results[i].owner_id;
				argGroupId=results[i].group_id;
				argSessionId=results[i].session_id;
				argImageName=results[i].image_name;
				argTopicId=results[i].topic_id;
				argTopicText=results[i].topic_text;
				argDepth=results[i].depth;

				let newArg=new Argument(argId, argType, argParentId, argText, argOwnerId, argGroupId, argSessionId, null, argImageName,argTopicId,argTopicText, argDepth);

				argArray.push(newArg);

			}

			console.log('Arguments loaded and sent to server from DB');



			return argArray;


		};

		this.FullGroupId= function(groupId)
		{
			var grupo = connection.query('SELECT * FROM Users WHERE group_id = '+groupId);
			var grupoIds = Array();
			for (i = 0; i < grupo.length; i++) {
				grupoIds.push(grupo[i].id);
			}
			return grupoIds;
		};

        this.loadChatLogs = function(groupId, topicId)
        {
            console.log("LOAD CHAT LOGS");
            var grupo = connection.query('SELECT * FROM Groups Where id = '+ groupId);
            console.log("LOAD CHAT LOGS ");
            console.log(grupo);
            //a eliminar sessionId
            var sessionId = 1;
            console.log('SELECT * FROM Chatlogs WHERE ( group_id = ' + groupId + ' AND session_id = '+sessionId +' AND argument_id = '+ topicId+ ')');
            var logs = connection.query('SELECT * FROM Chatlogs WHERE ( group_id = ' + groupId + ' AND session_id = '+sessionId +' AND argument_id = '+ topicId+ ')');
            console.log("LOAD CHAT LOGS wohfewofh");
            return logs;
        }


        this.loadArgGrupo=function(grupoId){

            argArray=Array();
            var results=connection.query('SELECT * FROM Arguments WHERE '
                +'(session_id = ' + session_id + ' AND group_id = ' +grupoId+
                ' )'+' ORDER BY id');

            var argId, argType, argParentId, argText, argOwnerId, argGroupId, argSessionId, argImageName, argTopicId,argTopicText, argDepth;

            for (var i=0; i<results.length; i++){

                argId=results[i].id;
                argType=results[i].type;
                argParentId=results[i].parent_id;
                argText=results[i].text;
                argOwnerId=results[i].owner_id;
                argGroupId=results[i].group_id;
                argSessionId=results[i].session_id;
                argImageName=results[i].image_name;
                argTopicId=results[i].topic_id;
                argTopicText=results[i].topic_text;
                argDepth=results[i].depth;

                let newArg=new Argument(argId, argType, argParentId, argText, argOwnerId, argGroupId, argSessionId, null, argImageName,argTopicId,argTopicText, argDepth);
                argArray.push(newArg);
            }

            console.log('Initial arguments loaded and sent to server from DB');
            return argArray;
        };


        //Dejara al grupo con una hora de FullGroup, la cual sera usada para ver cuando termina el timer
        this.setTimeFullGroup=function(groupId, time, voto2){
            var strPrevTime = connection.query('SELECT timefullgroup FROM Groups WHERE id = '+groupId)[0].timefullgroup;
            var prevTime = Number(strPrevTime);
            console.log("variavle prevTime");
            console.log(prevTime);
            if(prevTime==0 || voto2){
                console.log('UPDATE Groups SET timefullgroup = "'+time+'" WHERE id = '+groupId);
                connection.query('UPDATE Groups SET timefullgroup = "'+time+'" WHERE id = '+groupId);
                return time;
            }else{
                return prevTime;
            }
        };



		//Usada en el chat para generar los votones de votacion
		this.loadArgIniciales=function(grupoId){

			argArray=Array();

			var results=connection.query('SELECT * FROM Arguments WHERE '
				+'(session_id = ' + session_id + ' AND group_id = ' +grupoId+
				' AND type = 1)'+' ORDER BY id');

			var argId, argType, argParentId, argText, argOwnerId, argGroupId, argSessionId, argImageName, argTopicId,argTopicText, argDepth;

			for (var i=0; i<results.length; i++){

                argId=results[i].id;
                argType=results[i].type;
                argParentId=results[i].parent_id;
                argText=results[i].text;
                argOwnerId=results[i].owner_id;
                argGroupId=results[i].group_id;
                argSessionId=results[i].session_id;
                argImageName=results[i].image_name;
                argTopicId=results[i].topic_id;
                argTopicText=results[i].topic_text;
                argDepth=results[i].depth;
                let newArg=new Argument(argId, argType, argParentId, argText, argOwnerId, argGroupId, argSessionId, null, argImageName,argTopicId,argTopicText, argDepth);
				argArray.push(newArg);
			}

			console.log('Initial arguments loaded and sent to server from DB');
			return argArray;
		};

		//Usada en el chat, para guardar los mensajes en un log
		this.MsgSentLog=function(data)
		{
			var userId = data.userId;
			var groupId = data.groupId;
			var text =  data.text;
			var sessionId = connection.query('SELECT session_id FROM Groups WHERE id = ' + groupId )[0].session_id;
			var argumentId_array = connection.query('SELECT id FROM Arguments WHERE (group_id = '+ groupId
				+ ' AND session_id = '+ sessionId + ' AND type = 0 )');
			var argumentId = argumentId_array[0].id;
			console.log(argumentId);
			var timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
			console.log('INSERT INTO Chatlogs SET user_id='+userId+', group_id='+groupId+', text="'+text+'", session_id='+sessionId+', argument_id='+argumentId);
			connection.query('INSERT INTO Chatlogs SET user_id='+userId+', group_id='+groupId+', text="'+text+'", session_id='+sessionId+', argument_id='+argumentId);

			console.log('[ '+text + ' ]ingresado a ChatLogs');
		};

		this.loadUser= function(name, socket){


			var results = connection.query('SELECT * FROM Users WHERE name ="'+name+'"');

			var user=new User(results[0].id, results[0].name, results[0].group_id,results[0].assigned_to, socket);


			return user;



		};

		this.createQuestion=function({type, user, arg, argsArray, votesArray}){



			var results = connection.query('INSERT INTO Questions SET type='+type+', user_id='+user.id+', argument_id='+arg.id);

			var newQuestionId = results.insertId;

			var subArgs=this.findSubArgs(argsArray,arg,user.groupId);



			var question = new Question({id:newQuestionId, type:type, user:user, arg:arg, argsArray:subArgs, votesArray:votesArray, imageName:arg.imageName, topicId:arg.topicId, allArgs:argsArray});


			return question;


		};

		this.saveResponse=function(resp){


			var results = connection.query('INSERT INTO Responses SET question_id='+resp.id);
			var respID = results.insertId;
			console.log('Response '+respID+' to question '+resp.id+'added to DB');
			for (var j = 0; j < resp.respArgVote.length; j++){

				// Update the resp_args table
				var entry  = {response_id: respID.toString(), argument_id: resp.respArgID[j].toString(), argument_text: resp.respArgText[j], vote: resp.respArgVote[j].toString()};
				var results = connection.query('INSERT INTO Response_Arguments SET response_id='+respID+', argument_id='+resp.respArgID[j]+', argument_text="'+resp.respArgText[j]+'", vote='+resp.respArgVote[j]);


			}
			console.log(j+' response args added to DB');




		};


		this.createArgument=function(type, parentId, text, ownerId, groupId, sessionId, args, imageName, topicId, topicText, depth){


			var results = connection.query('INSERT INTO Arguments SET type='+type+', parent_id='+parentId+', text="'+text+'", owner_id='+ownerId+', group_id='+groupId+', session_id='+sessionId+', image_name="'+imageName+'", topic_id='+topicId+', topic_text="'+topicText+'", depth='+depth);

			var newArgId = results.insertId;
			console.log('Argument '+newArgId+' added to DB');
			var arg=new Argument(newArgId, type, parentId, text, ownerId, groupId, sessionId, args, imageName, topicId, topicText, depth);



			return arg;

		};

		this.createVote=function(userId, argId, argVote){


			console.log('INSERT INTO Votes SET user_id='+userId+', argument_id='+argId+', vote='+argVote);

			let results = connection.query('INSERT INTO Votes SET user_id='+userId+', argument_id='+argId+', vote='+argVote);

			let newVoteId=results.insertId;

			console.log('Vote from user ' + userId + ' on argument '+argId+' added to DB');

			let vote=new Vote({id:newVoteId, userId: userId, argumentId: argId, vote: argVote});

			return vote;

		};


		this.findSubArgs=function(argsArray,arg,groupId){
			subArgs=[];
			for (anArg of argsArray){
				if (anArg.parentId===arg.id && anArg.groupId===groupId){
					subArgs.push(anArg);

				}
			}
			return subArgs;


		};
	};
};