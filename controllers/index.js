
/*
 * Atiende a las solicitudes realizadas al home de la app.
 * Delega la resposabilidad a los otros controladores según corresponda.
 */
var express = require('express');
var router = express.Router();

//Todas las rutas que comiencen con /chat se delegan a su controlador.
//router.use('/chat', require('./chat'))


router.get('/', function(req, res) {

	var userArray=getNamesAndAssignments();
	userArray.then(function(userArray){
		res.render('index', {title: 'Actividad Colaborativa', userArray:userArray});
	});
});

router.get('/intro', function(req, res) {
	res.render('intro', {title: 'Actividad Colaborativa'});
});

router.get('/argsys', function(req, res) {
	res.render('argsys', {title: 'Actividad Colaborativa'});
});

router.get('/chat', function(req, res) {
	res.render('chat', {title: 'Actividad Colaborativa'});
});





function getNamesAndAssignments(){
    var p = new Promise(function(resolve, reject){
        var mysql2      = require('mysql');
        var connection2 = mysql2.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'root' ,
            //database:'old_db',
            database:'DB_2gr_1ses_6al_2pr',
            port: 3306,
            multipleStatements:true
        });
        connection2.query('SELECT name, assigned_to FROM Users', function (error,results,fields){
            if (error) throw error;
            var auxi = [];
            for(var i = 0; i<results.length; i++)
            {
                auxi.push([results[i].name,results[i].assigned_to]);
            }
            console.log(auxi);
            resolve(auxi);
        });
        /*var fs = require('fs');
        var arr1 = [];
        newDir = "assets/nombre.txt";
        var data = fs.readFileSync(newDir, 'utf8');
        var aux1 = data.split("\n");
        var arr = JSON.stringify(aux1);
        */
    });
    return p;
}

module.exports = router;