module.exports= function(connection, session_id, data){



	Help=function(){



		this.questionIrrational4=function(user, args, votes, argsArray){


			parentArg=argsArray[1];
			childArg=argsArray[0];


			var question=data.createQuestion({type:4, user:user, arg:parentArg, argsArray:args, votesArray:votes});
			question.childrenID.push(childArg.id);
			question.childrenType.push(childArg.type);
			question.childrenText.push(childArg.text);
			question.childrenVote.push(2);

			return question;







		};


		this.questionIrrational5=function(user, args, votes, argsArray){

			parentArg=argsArray[1];
			childArg=argsArray[0];


			var question=data.createQuestion({type:5, user:user, arg:parentArg, argsArray:args, votesArray:votes});
			question.childrenID.push(childArg.id);
			question.childrenType.push(childArg.type);
			question.childrenText.push(childArg.text);
			question.childrenVote.push(3);

			return question;


		};

		this.allUsersComplete=function(users){

			var allComplete=true;
			for (user of users){
				if (user.complete===false){
					allComplete=false;
				}

			}

			return allComplete;
		};


		this.irrationalArg4=function(user,args,votes){

			for (vote1 of votes){
				for (vote2 of votes){
					if (vote1!==vote2){

						let arg1=args.find(function(a){return a.id===vote1.argumentId;});
						let arg2=args.find(function(a){return a.id===vote2.argumentId;});
						if (vote1.userId===user.id && vote2.userId===user.id && arg1.parentId===arg2.id && arg1.type===3){

							if (vote1.vote===1 && vote2.vote===1){
								if (!vote1.votesAlreadyRationalized.includes(vote2) && !vote2.votesAlreadyRationalized.includes(vote1)){
									vote1.votesAlreadyRationalized.push(vote2);
									vote2.votesAlreadyRationalized.push(vote1);
									return [arg1,arg2];

								}

							}
						}

					}

				}
			}

			return false;




		};


		this.irrationalArg5=function(user,args,votes){

			for (vote1 of votes){
				for (vote2 of votes){
                    if (vote1!==vote2) {
                        let arg1 = args.find(function (a) {return a.id === vote1.argumentId;});
                        let arg2 = args.find(function (a) {return a.id === vote2.argumentId;});

                        if (vote1.userId === user.id && vote2.userId === user.id && arg1.parentId === arg2.id && arg1.type === 2) {

                            if (vote1.vote === 2 && vote2.vote === 1) {
                                if (!vote1.votesAlreadyRationalized.includes(vote2) && !vote2.votesAlreadyRationalized.includes(vote1)) {
                                    vote1.votesAlreadyRationalized.push(vote2);
                                    vote2.votesAlreadyRationalized.push(vote1);
                                    return [arg1, arg2];

                                }
                            }
                        }
                    }
				}
			}

			return false;





		};




		this.findFreeArg=function(args, user, votes){ //si hay un argumento que no está ocupado y no se ha votado por él, se devuelve

        	//search first args from current topic
        	var sortedArgs=args.filter(x=>x.topicId===user.currentTopic).sort((a,b)=>a.id-b.id).concat(args.filter(x=>x.topicId!==user.currentTopic).sort((a,b)=>a.id-b.id));

			console.log('SORTEDORDER', Array.from(sortedArgs,function(u){return u.id;}));

        	for (arg of sortedArgs){
				if (arg.ownerId!==user.id) {
					if (arg.busy === false) {
						if (arg.type !== 0) {

							if (arg.groupId === user.groupId) {

								var notVoted = true;

								for (vote of votes) {
									if (vote.userId === user.id && vote.argumentId === arg.id) {
										notVoted = false;
									}

								}

								if (notVoted) {
									return arg;
								}
							}


						}

					}
				}

			}
			return false;

		};


		this.checkIfAllUsersVotedForTopic=function(user, users, votes, args, currentTopic){

			var usersInSameGroup=users.filter(x=>x.groupId===user.groupId);

			console.log('1USERSINSAMEGROUP', usersInSameGroup);

			var allUsersVoted=true;

			for (var otherUser of usersInSameGroup){



				var argsForCurrentTopic=args.filter(x=>x.topicId===currentTopic && x.type!==0);

				console.log('1ARGSFORCURRENTTOPIC', argsForCurrentTopic);

				var votesOfOtherUser= votes.filter(x=>x.userId===otherUser.id);

				console.log('1VOTESOFOTHERUSER', votesOfOtherUser);


				var allArgsVotedForUser=true;

				for (var arg of argsForCurrentTopic){

					var argVoted= false;

					for (var vote of votesOfOtherUser){

						console.log('POSSIBLEMATCH',arg.id, vote.argumentId);

						if (arg.id===vote.argumentId && arg.busy===false){
							argVoted=true;
							console.log('ARGVOTED', arg, vote);

						}

					}




					allArgsVotedForUser=allArgsVotedForUser && argVoted;

					console.log('UPDATEallArgsVotedForUser', allArgsVotedForUser);

				}
				allUsersVoted=allUsersVoted && allArgsVotedForUser;

				console.log('UPDATEallUsersVoted', allUsersVoted);


			}

			return allUsersVoted;
		};




		this.checkIfUserVotedForSpecificArg=function(user, arg, votes){


			var votesOfUser= votes.filter(x=>x.userId===user.id);

			var argVoted= false;

			for (var vote of votesOfUser){

				if (arg.id===vote.argumentId){argVoted=true;}
			}

			return argVoted;




		};


		this.addNewArguments=function(argIds, argTexts, argVotes, imageName, topicId, topicText, args, user){
			let newArgs=[];
			let existingArgsTexts=[];

			let parentId=argIds[0];

			for (i=1; i<argIds.length; i++){

				let argText=argTexts[i];
				let type=argVotes[i]+1; //type=2 if vote==1, type=3 if vote==2

				let repeated=false;

				for (iArg of args){
					if (iArg.text===argText) {
                        console.log("=== funciona, viste");
						repeated=true;
						existingArgsTexts.push(argText)

                    }


				}

				if (!repeated) {

					let newArg = data.createArgument(type, parentId, argText, user.id, user.groupId, session_id, args, imageName, topicId, topicText, arg.depth+1); //2 a favor, 3 en contra

					newArgs.push(newArg);

				}

			}

			return [newArgs, existingArgsTexts];

		};


		this.addNewVotes=function(user, newArgs, existingArgsTexts, args){
			let newVotes=[];
			let existingArgs=[];
            console.log("Existing argsTexts: "+existingArgsTexts);
            for (text of existingArgsTexts){
                existingArgs.push(args.find(function (x) {return x.text===text}));
            }

            console.log("Existing args: "+existingArgs);


			for (arg of newArgs.concat(existingArgs)){


				let voteType=arg.type-1;

				let vote=data.createVote(user.id, arg.id, voteType);

				newVotes.push(vote);

			}



			return newVotes;

		};


		this.questionsAccordingToVote=function(voteType, user, arg, args, votes){
			let questionArray=[];
			let question;

			if (voteType===1){
				question=data.createQuestion({type:2, user:user, arg:arg, argsArray:args, votesArray:votes, imageName:arg.imageName, topicId:arg.topicId});
				questionArray.push(question);
				console.log('Server sent ' + user.name + ' question type ' + 2);
			}
			else if (voteType===2){
				question=data.createQuestion({type:3, user:user, arg:arg, argsArray:args, votesArray:votes, imageName:arg.imageName, topicId:arg.topicId});
				questionArray.push(question);
				console.log('Server sent ' + user.name + ' question type ' + 3);
			}
			else if (voteType===3){
				let questionOne=data.createQuestion({type:2, user:user, arg:arg, argsArray:args, votesArray:votes, imageName:arg.imageName, topicId:arg.topicId});

				questionArray.push(questionOne);

				let questionTwo=data.createQuestion({type:3, user:user, arg:arg, argsArray:args, votesArray:votes, imageName:arg.imageName, topicId:arg.topicId});

				questionArray.push(questionTwo);

				console.log('Server sent ' + user.name + ' question type ' + 2 +' and put question '+3 +' on queue');
			}
			return questionArray;
		};



		this.changeIrrationalVotes=function(votes, parentId, childId, change){

			if (change==='change-parent-no'){
				let voteToChange=votes.find(function (vote) {return vote.argumentId===resp.respArgID;});
				voteToChange.vote=2;

			}
			else if (change==='change-parent-yes'){
				let voteToChange=votes.find(function (vote) {return vote.argumentId===resp.respArgID;});
				voteToChange.vote=1;

			}
			else if (change==='change-child-no'){
				let voteToChange=votes.find(function (vote) {return vote.argumentId===resp.childrenID[0];});
				voteToChange.vote=2;

			}
			else if (change==='change-child-yes'){
				let voteToChange=votes.find(function (vote) {return vote.argumentId===resp.childrenID[0];});
				voteToChange.vote=1;

			}

			return votes;
		};


		this.userVotedForAllArgs=function(user, args, votes){

            let votesOfUser=votes.filter(vote=>vote.userId===user.id);

            let arrayOfUsersVotesArgIds=votesOfUser.map(vote=>vote.argumentId);


            let allArgsVoted=true;

            for (let arg of args){
                if (arg.type!==0 && arg.groupId===user.groupId){
                    if (!arrayOfUsersVotesArgIds.includes(arg.id)) allArgsVoted=false;
                }

            }


            return allArgsVoted;
        };













	};




};