var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;


//CLASSES

module.exports = function(server, connection, session_id) {
	// Initial stuff

	var nSession = session_id;

	var group_ids = [];
	var group_sockets = [];
	var nGroups;
	var args = [];
	var users = [];
	var qus = [];
	var nMaxUsers = 3;
	var listFFUsers = [];
	var listFFArgs = [];
	var listUserIDs = [];
	//var listUserGroups = [];
	var listUserNums = [];




	function Argument(argID, type, text, parentID, votes,args){

		this.id = argID;
		this.type = type;
		this.text = text;
		this.parentID = parentID;
		this.busy = false;
		console.log('Argument ' + this.id + ' is not busy ' + this.busy); // delete
		this.qUserID = [];
		this.qQuType = [];
		this.votes = new Array(votes.length);
		for (var t = 0; t < votes.length; t++){     // TO DO: NEEDS FIXING FOR VOTE ORDER
			this.votes[t] = votes[t];
		}
		if (type == 1){
			this.level = 1;
		}
		else{
			for (var iu = 0; iu < args.length; iu++){

				var b = args[iu];

				if (b.id == this.parentID){

					this.level = b.level +1;
					break;

				}
			}
		}


	}

	function Question(id, nQu, type, nArg, nUser, nGroup, args, users){

		this.id = id;
		this.nQu = nQu;
		this.type = type;
		this.nArg = nArg;
		this.nUser = nUser;
		this.nGroup = nGroup;
		console.log(users); // delete
		console.log(nGroup); // delete
		console.log(nUser); // delete
		console.log(users[nGroup][nUser]); // delete
		this.userID = users[nGroup][nUser].id;
		this.argID = args[nGroup][nArg].id;
		this.argType = args[nGroup][nArg].type;
		this.argText = args[nGroup][nArg].text;
		this.argVote = args[nGroup][nArg].votes[nUser];
		this.childrenID = [];
		this.childrenType = [];
		this.childrenText = [];
		this.childrenVote = [];

		if (type != 1){

			for (var ig = 0; ig < args[nGroup].length; ig++){

				var b = args[nGroup][ig];

				if (b.parentID == this.argID){

					this.childrenID.push(b.id);
					this.childrenType.push(b.type);
					this.childrenText.push(b.text);
					this.childrenVote.push(b.votes[nUser]);

				}
			}

		}

		this.respArgID = [];
		this.respArgText = [];
		this.respArgVote = [];

	}

	function log(user_id, message, nSession, connection)
	{
		var entry  = {user_id: user_id, session: nSession, message: message};
		var query = connection.query('INSERT INTO Logs SET ?', entry, function(error, results) {
			if (error) throw error;
		});
	}


	function User(name, id, users, groupID, nGroup, socket){

		this.name = name;
		this.busy = false;
		this.complete = false;
		this.socketID = socket.id;
		this.id = id;
		this.groupID = groupID;
		this.nGroup = nGroup;
		this.nUser = users[nGroup].length;

	}


	//FUNCTIONS


	// Finds the next question for a given user
	function FindNextQu(nUser, nGroup, users, args){

		var p = new Promise(function(resolve, reject){

			var blAnsIncomplete = false;
			var blProsConsIncomplete = false;
			var blFoundResolve = false;
  

			console.log('Finding next Qu for ' + users[nGroup][nUser].name); // delete

			for (var s = 0; s < args[nGroup].length; s++){
      

				if (args[nGroup][s].type == 1){

					if (args[nGroup][s].votes[nUser] == 0) {
          
						if (args[nGroup][s].busy == false){
							console.log('Argument ' + args[nGroup][s].id + ' is not busy'); // delete

							blFoundResolve = true;
							resolve([1, s]);
							break;
						}
						else{
							blAnsIncomplete = true;
							if (args[nGroup][s].qUserID.indexOf(users[nGroup][nUser].id) == -1){
								args[nGroup][s].qUserID.push(users[nGroup][nUser].id);
								args[nGroup][s].qQuType.push(1);              
							}

						}

					}
				}

			}

			if (blAnsIncomplete){
				// Wait for first available arg? 
				blFoundResolve = true;
				resolve([0, []]);
			}
			else if (blFoundResolve != true){ 
				// no more questions on ans, now for pros and cons
				for (var q = 0; q < args[nGroup].length; q++){

					if (args[nGroup][q].type == 2 || args[nGroup][q].type == 3){

						if (args[nGroup][q].votes[nUser] == 0) {

							if (args[nGroup][q].busy == false){
								blFoundResolve = true;
								resolve([1, q]);
								break;
							}
							else{
								bProsConsIncomplete = true;
								if (args[nGroup][q].qUserID.indexOf(users[nGroup][nUser].id) == -1){
									args[nGroup][q].qUserID.push(users[nGroup][nUser].id);
									args[nGroup][q].qQuType.push(1);
								}

							}

						}

					}
				}

				if (blProsConsIncomplete && blFoundResolve == false){
					// Wait for first available arg? 
					resolve([0, []]);
				}
				else if(blFoundResolve == false){
        
					// Check for type 4/5 questions
					for (var ih = 0; ih < args[nGroup].length; ih++){

						// Check whether the user has already been asked a type 4/5 question for this arg
						var alreadyAsked = false;
						for (var ij = 0; ij < listFFArgs.length; ij++){
							if (listFFArgs[ij] == args[nGroup][ih].id && listFFUsers[ij] == users[nGroup][nUser].id){
								var alreadyAsked = true;
							}

						}

						// Only type 4/5 for answer arguments for now
						if (args[nGroup][ih].type == 1 && alreadyAsked == false){

							// If user votes for or against, check children
							if (args[nGroup][ih].votes[nUser] == 1) {

								var blVotesForSupp = false;
								var blVotesForAtt = false;

								for (var ii = 0; ii < args[nGroup].length; ii++){

									if (args[nGroup][ii].parentID == args[nGroup][ih].id){
										// If user votes for supporter of parent arg
										if (args[nGroup][ii].votes[nUser] == 1 && args[nGroup][ii].type == 2){
											blVotesForSupp = true;
										}
										// If user votes for attacker of parent arg
										else if (args[nGroup][ii].votes[nUser] == 1 && args[nGroup][ii].type == 3){
											blVotesForAtt = true;
										}

									}
								}

								// If the user votes for an attacker but not for a supporter, qu 4
								if (blVotesForSupp == false && blVotesForAtt == true) {

									// Check whether arg is free and add to queue if not
									if (args[nGroup][ih].busy == false){
										blFoundResolve = true;
										resolve([4, ih]);
										break;
									}
									else{
										blAnsIncomplete = true;
										args[nGroup][ih].qUserID.push(users[nGroup][nUser].id);
										args[nGroup][ih].qQuType.push(4);
										blFoundResolve = true;
										resolve([0, []]); 
										break;  // User can only be in one queue for type 4/5
									}

								}


							}

							else if (args[nGroup][ih].votes[nUser] == 2) {

								var blVotesForSupp = false;
								var blVotesForAtt = false;

								for (var ii = 0; ii < args[nGroup].length; ii++){

									if (args[nGroup][ii].parentID == args[nGroup][ih].id){
										// If user votes for supporter of parent arg
										if (args[nGroup][ii].votes[nUser] == 1 && args[nGroup][ii].type == 2){
											blVotesForSupp = true;
										}
										// If user votes for attacker of parent arg
										else if (args[nGroup][ii].votes[nUser] == 1 && args[nGroup][ii].type == 3){
											blVotesForAtt = true;
										}

									}
								}

								// If the user votes for a supporter but not for an attacker, qu 5
								if (blVotesForSupp == true && blVotesForAtt == false) {

									// Check whether arg is free and add to queue if not
									if (args[nGroup][ih].busy == false){
										blFoundResolve = true;
										resolve([5, ih]);
										break;
									}
									else{
										blAnsIncomplete = true;
										args[nGroup][ih].qUserID.push(users[nGroup][nUser].id);
										args[nGroup][ih].qQuType.push(5);
										blFoundResolve = true;
										resolve([0, []]); 
										break;  // User can only be in one queue for type 4/5
									}
                
								}

							}
						}

					}

					if (blFoundResolve == false){

						// No more questions
						resolve([-1,[]]);
					}

				}

			}

		});

  
		return p;
	}



	//PROGRAM LOGIC


	//INITIAL CHARGING
  
	////Charge users from txt
	//var txtFile = "./userRegistry.txt";
	//var file = new File(txtFile);
	//var lines = file.split("\n");
	//if (lines.length>=3){
	//  // parse phrases
	//  for (var i = 0; i < lines.length; i++) {
	//    var line = lines[i];
	//    var userEntry  = {id: i, name:line, group_id: parseInt((i-i%3)/3,10)};
	//    var userQuery = connection.query('INSERT INTO Users SET ?', entry, function(error, results) {
	//      if (error) throw error;
	//    });
	//
	//    
	//  }
	//}
	//
	////Charge questions from txt
	//var txtFile = "./questionsRegistry.txt";
	//var file = new File(txtFile);
	//var lines = file.split("\n");
	//if (lines.length>=3){
	//  // parse phrases
	//  for (var i = 0; i < lines.length; i++) {
	//    var line = lines[i];
	//    if (line.startsWith("-")){
	//      var questionEntry  = {};
	//      var questionQuery = connection.query('INSERT INTO Users SET ?', entry, function(error, results) {
	//        if (error) throw error;
	//      });
	//    }
	//    
	//  }
	//}

	var maxGroups=0;
	function readUsers(file) {
		var rawFile = new XMLHttpRequest();
		rawFile.open('GET', file, true);
		rawFile.onreadystatechange = function ()
		{
			if(rawFile.readyState === 4)
			{
				if(rawFile.status === 200 || rawFile.status == 0)
				{
					var allText = rawFile.responseText;

					var lines = allText.split('\n');
					if (lines.length>=3){
						// parse phrases
						for (var i = 0; i < lines.length; i++) {
							var line = lines[i];
							if (line.startsWith('-')){
								var groupId=parseInt((i-i%3)/3,10);
								var maxGroups=groupId;
								var userEntry  = {id: i, name:line, group_id: groupId};
								var userQuery = connection.query('INSERT INTO Users SET ?', entry, function(error, results) {
									if (error) throw error;
								});
							}
                    
						}
					}
				}
			}
		};
		rawFile.send(null);
	}

	function readQuestions(file) {
		var rawFile = new XMLHttpRequest();
		rawFile.open('GET', file, false);
		rawFile.onreadystatechange = function ()
		{
			if(rawFile.readyState === 4)
			{
				if(rawFile.status === 200 || rawFile.status == 0)
				{
					var allText = rawFile.responseText;
					var lines = allText.split('\n');
					if (lines.length>=3){
						// para todos los grupos rellenamos los argumentos
						for (var j=0;j<=maxGroups;j++){
							var iArg=0;
							for (var i = 0; i < lines.length; i++) {
								var line = lines[i];
								if (line.startsWith('-')){ //argumentos respuesta
									var argEntry  = {id: i, type: 1, text: line.slice(1), group_id: j, session_id: nSession};
									var argQuery = connection.query('INSERT INTO Arguments SET ?', argEntry, function(error, results) {
										if (error) throw error;
									});
								} else { //argumentos tópicos
									var topicEntry  = {id: i, type: 0, parent_id: iArg, text: line, group_id: j, session_id: nSession};
									var topicQuery = connection.query('INSERT INTO Arguments SET ?', topicEntry, function(error, results) {
										if (error) throw error;
									});
									iArg+=1;


								}
                      
							}
						}
					}
				}
			}

		};
		rawFile.send(null);
	}

	//readUsers("http://localhost:3000/assets/userRegistry.txt");
	//readQuestions("http://localhost:3000/assets/questionsRegistry.txt")

	// CHANGE THIS
  

  
	//console.log(socket);

	// Query the database for group ids and sockets
	connection.query('SELECT * FROM Groups WHERE session_id = ' + nSession + ' ORDER BY id', function (error,results,fields){
    
		if (error) throw error;

		nGroups = results.length;

		for(var nGroup = 0; nGroup < nGroups; nGroup++)
		{

			// Ensures the args and users variables are the correct dimensions
			args.push([]);
			users.push([]);

			group_ids.push(results[nGroup].id);
			group_sockets.push(results[nGroup].socket);

		}

		// for each group, instantiate the arguments
		connection.query('SELECT * FROM Arguments WHERE session_id = ' + nSession, function (error,resultsa,fields){

			if (error) throw error;

			var nDBArgs = new Array(nGroups);

			for(var nGroup = 0; nGroup < nGroups; nGroup++)
			{

				nDBArgs[nGroup] = 0;

				for (var ic=0;ic<resultsa.length;ic++){

					if (resultsa[ic].group_id == group_ids[nGroup]){

						var argID = resultsa[ic].id;
						var type = resultsa[ic].type;
						var text = resultsa[ic].text;
						var parentID = resultsa[ic].parent_id;
						var ownerID = resultsa[ic].owner_id;
						var votes = new Array(nMaxUsers);
						for (var j=0;j<votes.length;j++){
							votes[j] = 0;
						}


						var tempArg = new Argument(argID, type, text, parentID, votes, args);


						args[nGroup].push(tempArg);

						nDBArgs[nGroup]++;

					}

				}

				console.log(nDBArgs[nGroup] + ' args added from initial DB for group id ' + group_ids[nGroup]);

			}

			console.log(args);  // delete

		});




	});




	var io = require('socket.io').listen(server);

	io.sockets.on('connection', function (socket) {

		//require('../controllers/game.js')(io, socket, connection);

		socket.on('UserConnected', function(name){

			console.log('server sees user ' + name + ' is connected');

			connection.query('SELECT * FROM Users WHERE name ="'+name+'"', function (error,results,fields){
            
				if (error) throw error;

				var userID = results[0].id;
				var groupID = results[0].group_id;

				// Find the group_no using the group_id
				if (group_ids.indexOf(groupID) == -1){
					console.log('Cannot find group ID ' + groupID + ' in system');
				} 
				else{
					var nGroup = group_ids.indexOf(groupID);
				}

				// Check user is not already in the system and it was a disconnection
				if (listUserIDs.indexOf(userID) == -1){
					var u = new User(name, userID, users, groupID, nGroup, socket);
					//u.id = userID;
					console.log(u.name + ' (ID: ' + userID + ') user found in DB, new user added');
					users[nGroup].push(u);
					var nUser = u.nUser;
					listUserIDs.push(userID);
					listUserNums.push(nUser);
					//listUserGroups.push(nGroup);
				}
				else{
					var nUser = listUserNums[listUserIDs.indexOf(userID)];
					console.log('Old user reconnected');
				}

            

				// Check if there are any more questions for the user

				var aux = FindNextQu(nUser, nGroup, users, args);

				aux.then(function(NextQu){


					//var NextQu = FindNextQu(u, users, args);
					var nArg = NextQu[1];
              

					// If no more questions
					if (NextQu[0] == -1) {
						users[nGroup][nUser].complete = true;
						//u.busy = false;
						console.log(users[nGroup][nUser].name + ' is complete');
						// If more questions but they are currently busy  
					}else if (NextQu[0] == 0) {
						//u.busy = false;
						console.log(users[nGroup][nUser].name + ' is waiting for a free arg');
						// If type 1/4/5 questions  
					} else {
                
						console.log('First Qu for user '+ users[nGroup][nUser].name +' on arg '+args[nGroup][nArg].id+' of type '+NextQu[0]);

              
						args[nGroup][nArg].busy = true;
						users[nGroup][nUser].busy = true;

						console.log('Argument ' + args[nGroup][nArg].id + ' is busy' + args[nGroup][nArg].busy); // delete

						tempType = NextQu[0];

						// Create new Type 4/5 Qu in the DB
						var entry  = {type: tempType, user_id: users[nGroup][nUser].id, argument_id: args[nGroup][nArg].id};
						var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {

							if (error) throw error;
							var newQuID = results.insertId;
                  
							var qu = new Question(newQuID, qus.length, tempType, nArg, nUser, nGroup, args, users);
							qus.push(qu);
							log(users[nGroup][nUser].id, ' qu type ' + tempType, nSession,connection);
							io.to(users[nGroup][nUser].socketID).emit('Qu', qu);

							console.log('server sent ' + users[nGroup][nUser].name + ' qu type ' + tempType);

						});


                
					}

				});

			});

          
		});


		socket.on('Response', function(resp) {

			console.log('Type '+resp.type+ ' response received from user '+resp.userID+' on arg '+resp.argID + ' from group ' + resp.nGroup);
			log(resp.userID, 'Type '+resp.type+ ' response received from user '+resp.userID+' on arg '+resp.argID + ' from group ' + resp.nGroup,nSession,connection);
			var nGroup = resp.nGroup;

			// Find the ref number of the main argument from the response
			for (var nArg = 0; nArg < args[nGroup].length; nArg++){

				if (args[nGroup][nArg].id == resp.argID){
					break;
				} 
			}


			// Set nUser to be the reference of the sender of the response
			for (var nUser = 0; nUser < users[nGroup].length; nUser++){

				if (users[nGroup][nUser].id == resp.userID){
					break;
				} 
			}

			// Keeps a record of the type 4 and 5 questions, so we don't ask them more than once
			if (resp.type == 4 || resp.type == 5){

				listFFArgs.push(args[nGroup][nArg].id);
				listFFUsers.push(users[nGroup][nUser].id);
            
			}


			// Update the response table and the resp_args
			var entry  = {question_id: resp.id};
			var query = connection.query('INSERT INTO Responses SET ?', entry, function(error, results) {
				if (error) throw error;
				var respID = results.insertId;
				console.log('Response '+respID+' to qu '+resp.id+'added to DB');
				for (var j = 0; j < resp.respArgVote.length; j++){

					// Update the resp_args table
					var entry  = {response_id: respID, argument_id: resp.respArgID[j], argument_text: resp.respArgText[j], vote: resp.respArgVote[j]};
					var query = connection.query('INSERT INTO Response_Arguments SET ?', entry, function(error, results) {
						if (error) throw error;
					});

				}
				console.log(j+' response args added to DB');

			});



			var listOfPromises = [];
			var argsAdded = [];

			// Update the system and database arguments and votes
			for (var i = 0; i < resp.respArgVote.length; i++){


				// If the arg doesn't have an id then it is a new one
				if (resp.respArgID[i] == -1){

					if (resp.type == 2 || resp.type == 4){
						var tempType = 2;
					}
					else if (resp.type == 3 || resp.type == 5){
						var tempType = 3;
					}

					// Create new argument in system
					var newArgVotes = Array(nMaxUsers);
					for (var j=0;j<newArgVotes.length;j++){
						newArgVotes[j] = 0;
					}
					newArgVotes[nUser] = 1;
					var b = new Argument(-1, tempType, resp.respArgText[i], args[nGroup][nArg].id, newArgVotes, args); // ID updated from -1 in following query

					// Create new argument in database
					var pa = new Promise(function(resolve, reject){
						var entry  = {type: tempType, parent_id: args[nGroup][nArg].id, text: resp.respArgText[i], owner_id: users[nGroup][nUser].id, group_id: group_ids[nGroup], session_id: nSession};
						//var tempUserID = users[nUser].id;

						var query = connection.query('INSERT INTO Arguments SET ?', entry, function(error, results) {
							if (error) throw error;
							var newArgIDa = results.insertId;
							console.log('Argument '+newArgIDa+' added to DB');
							b.id = newArgIDa;
							args[nGroup].push(b);
							argsAdded.push(newArgIDa);
							resolve(newArgIDa);

							// Add vote from user to db
							var entry  = {user_id: users[nGroup][nUser].id, argument_id: newArgIDa, vote: 1};
							var query = connection.query('INSERT INTO Votes SET ?', entry, function(error, results) {
								if (error) throw error;

								console.log('Vote from user ' + users[nGroup][nUser].name + ' on argument '+newArgIDa+' added to DB'); // delete
                  

							});

						});

						return pa;
					});

					listOfPromises.push(pa);
              

				}
				// If the arg has an ID then the votes need to be checked
				else{

					// Change argument's vote in system
					for (var r = 0; r < args[nGroup].length; r++){

						if (resp.respArgVote[i] != -1 && resp.respArgVote[i] != 0){

							if (args[nGroup][r].id == resp.respArgID[i]){

								// see below
								/*var blUpdate = true;
                    if (args[r].votes[u.nUser] == 0){
                      blUpdate = false;
                    }*/


								args[nGroup][r].votes[nUser] = resp.respArgVote[i];

								console.log('User '+users[nGroup][nUser].name+' vote on arg '+resp.respArgID[i]+' updated');
                    
								// If it is a type 2 or 3 qu then the first arg's vote does not need changing
								if ((resp.type != 2 && resp.type != 3) || i != 0){
									var qa = i;
									var entry  = {user_id: users[nGroup][nUser].id, argument_id: resp.respArgID[qa], vote: resp.respArgVote[qa]};
									var query = connection.query('INSERT INTO Votes SET ?', entry, function(error, results) {
										if (error) throw error;
										console.log('Vote from user ' +users[nGroup][nUser].name+' on Argument '+resp.respArgID[qa]+' added to DB');
									});
								}


                    


								break;        

							}
						}
					}

					// OLD VERSION WITH ASYNCHONOUS ISSUES
					// Check to see if there is already an entry for the argument
					//connection.query('SELECT * FROM Votes WHERE user_id ="'+u.id+'" AND argument_id ="'+argID[qa]+'"', function (error,results,fields){
					//  if (error) throw error;

                
					//  }
					//});
				}
			}


			Promise.all(listOfPromises).then(values =>{


				console.log('Next Questions'); // delete

				// If Type 1 Q for an Ans arg then check response 
				if (resp.type == 1 && args[nGroup][nArg].level < 3){
              
					console.log('Type 2 and 3 Questions');

					if (args[nGroup][nArg].votes[nUser] == 1){
						// Create new Type 2 Qu in the DB
						var entry  = {type: 2, user_id: users[nGroup][nUser].id, argument_id: args[nGroup][nArg].id};
						var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
							if (error) throw error;
							var newQuID = results.insertId;

							var qu = new Question(newQuID, qus.length, 2, nArg, nUser, nGroup, args, users);
							qus.push(qu);
							io.to(users[nGroup][nUser].socketID).emit('Qu', qu);
							log(users[nGroup][nUser].id, ' qu type 2',nSession,connection);
							console.log('server sent ' + users[nGroup][nUser].name + ' qu type ' + 2);
						});
                
					}
					else if (args[nGroup][nArg].votes[nUser] == 2){
						// Create new Type 3 Qu in the DB
						var entry  = {type: 3, user_id: users[nGroup][nUser].id, argument_id: args[nGroup][nArg].id};
						var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
							if (error) throw error;
							var newQuID = results.insertId;

							var qu = new Question(newQuID, qus.length, 3, nArg, nUser, nGroup, args, users);
							qus.push(qu);
							io.to(users[nGroup][nUser].socketID).emit('Qu', qu);
							console.log('server sent ' + users[nGroup][nUser].name + ' qu type ' + 3);
						});
                
					}
					else {
						// Create new Type 2 Qu in the DB
						var entry  = {type: 2, user_id: users[nGroup][nUser].id, argument_id: args[nGroup][nArg].id};
						var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
							if (error) throw error;
							var newQuID = results.insertId;
							var qu = new Question(newQuID, qus.length, 2, nArg, nUser, nGroup, args, users);
							qus.push(qu);
							io.to(users[nGroup][nUser].socketID).emit('Qu', qu);
							log(users[nGroup][nUser].id, ' qu type 2 after ? vote',nSession,connection);
							console.log('server sent ' + users[nGroup][nUser].name + ' qu type ' + 2 + ' after ? vote');
						});
                
					}

					return;
				}

				// If Type 2 Q for an Ans arg then and response to Ans Arg was ?, send the Type 3 Q 
				else if (resp.type == 2 && args[nGroup][nArg].type == 1 && args[nGroup][nArg].votes[nUser] == 3) {

					console.log('Type 3 (Part 2) Question');


					// Create new Type 3 Qu in the DB
					var entry  = {type: 3, user_id: users[nGroup][nUser].id, argument_id: args[nGroup][nArg].id};
					var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
						if (error) throw error;
						var newQuID = results.insertId;
						var qu = new Question(newQuID, qus.length, 3, nArg, nUser, nGroup, args, users);
						qus.push(qu);
						io.to(users[nGroup][nUser].socketID).emit('Qu', qu);
						log(users[nGroup][nUser].id, ' qu type 3 after ? vote',nSession,connection);
						console.log('server sent ' + users[nGroup][nUser].name + ' qu type ' + 3 + ' after ? vote');
					});
              

					return;

				}

				// Remove user from the queue of this arg if they were in it
				var index = args[nGroup][nArg].qUserID.indexOf(users[nGroup][nUser].id);
				if (index >= 0){
					args[nGroup][nArg].qUserID.splice(index, 1);
					args[nGroup][nArg].qQuType.splice(index, 1);
				}

				// look for any free users who are in the queue
				args[nGroup][nArg].busy = false;
				console.log('Argument ' + args[nGroup][nArg].id + ' is not busy'); // delete

				for (var n = 0; n < args[nGroup][nArg].qUserID.length; n++){

					var uID = args[nGroup][nArg].qUserID[n];

					// Find the user with that ID
					for (var nUserQ = 0; nUserQ < users[nGroup].length; nUserQ++){

						if (users[nGroup][nUserQ].id == uID){
							break;
						} 
					}


					if (users[nGroup][nUserQ].busy == false){

						console.log('Questions to users in the queue');

						args[nGroup][nArg].busy = true;
						users[nGroup][nUserQ].busy = true;

						console.log('Argument ' + args[nGroup][nArg].id + ' is busy' + args[nGroup][nArg].busy); // delete

						// Create new Type X Qu in the DB
						var entry  = {type: args[nGroup][nArg].qQuType[n], user_id: users[nGroup][nUserQ].id, argument_id: args[nGroup][nArg].id};
						var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
							if (error) throw error;
							var newQuID = results.insertId; 
							var qu = new Question(newQuID, qus.length, args[nGroup][nArg].qQuType[n], nArg, nUserQ, nGroup, args, users);
							qus.push(qu);
							io.to(users[nGroup][nUserQ].socketID).emit('Qu', qu);
							log(users[nGroup][nUser].id, ' from the queue qu type ' + args[nGroup][nArg].qQuType[n],nSession,connection);
							console.log('server sent ' + users[nGroup][nUserQ].name + ' from the queue qu type ' + args[nGroup][nArg].qQuType[n]);
						});

						if (args[nGroup][nArg].qQuType[n] == 4 || args[nGroup][nArg].qQuType[n] == 5){

							listFFArgs.push(args[nGroup][nArg].id);
							listFFUsers.push(users[nGroup][nUserQ].id);

						}

						break;

					}
				}

				// Check if there are any more questions for the user
				users[nGroup][nUser].complete = false;

				var aux = FindNextQu(nUser, nGroup, users, args);

				aux.then(function(NextQu){

					// If no more questions
					if (NextQu[0] == -1) {
						users[nGroup][nUser].complete = true;
						users[nGroup][nUser].busy = false;

						console.log(users[nGroup][nUser].name + ' is complete');

						// Check for all users being complete
						blDone = true;
						for (ip = 0; ip < users[nGroup].length; ip++){
                  
							if (users[nGroup][ip].complete == false){
								blDone = false;
								break;
							}
						}
						if (blDone){
							// Emit message to users that it is complete
							for (iq = 0; iq < users[nGroup].length; iq++){
                  
								io.to(users[nGroup][iq].socketID).emit('complete');
								log(users[nGroup][nUser].id, ' Debate Complete ',nSession,connection);
							}
							console.log('Debate Complete for Group ID ' + group_ids[nGroup]);
						}


						// If more questions but they are currently busy  
					}else if (NextQu[0] == 0) {
						users[nGroup][nUser].busy = false;
						console.log(users[nGroup][nUser].name + ' is waiting for a free arg');
						// If type 1/4/5 questions    
					} else {

						console.log('Next Type 1, 4 or 5 Question');

						var nArg = NextQu[1];

						args[nGroup][nArg].busy = true;
						users[nGroup][nUser].busy = true;

						console.log('Argument ' + args[nGroup][nArg].id + ' is busy' + args[nGroup][nArg].busy); // delete

						// Create new Type X Qu in the DB
						var tempType = NextQu[0];
						var entry  = {type: tempType, user_id: users[nGroup][nUser].id, argument_id: args[nGroup][nArg].id};
						var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
							if (error) throw error;
							var newQuID = results.insertId; 
							var qu = new Question(newQuID, qus.length, tempType, nArg, nUser, nGroup, args, users);
							qus.push(qu);
							io.to(users[nGroup][nUser].socketID).emit('Qu', qu);
							log(users[nGroup][nUser].id, ' qu type ' + tempType,nSession,connection);
							console.log('server sent ' + users[nGroup][nUser].name + ' qu type ' + tempType);
						});

                


        


					}


				});

				// If arguments were added, send them to complete users

				for (il = 0; il < argsAdded.length; il++){

					// Find the added arg in args
					var addedArgID = argsAdded[il];

					console.log('Checking complete users for arg ID ' + addedArgID); // delete

					for (im = 0; im < args[nGroup].length; im++){

						if (args[nGroup][im].id == addedArgID){
							var addedArgNo = im;
							console.log('Added argument is no. ' + addedArgNo + ' and ID ' + addedArgID); // delete
							break;
						}
					}

					var blMsgSent = false;
					console.log('Length of users = ' + users[nGroup].length); // delete 
					for (ik = 0; ik < users[nGroup].length; ik++) {

						if (users[nGroup][ik].complete == true && ik != nUser){

							users[nGroup][ik].complete = false;

							if (blMsgSent){

								// Add to queue
								args[nGroup][addedArgNo].qUserID = users[nGroup][ik].id;
								args[nGroup][addedArgNo].qQuType = 1;
							}
							else{

								// Send Message and update db
								args[nGroup][im].busy = true;

								console.log('Sending qu for user ' + users[nGroup][ik].name); // delete 
								console.log('Argument ' + args[nGroup][addedArgNo].id + ' is busy' + args[nGroup][addedArgNo].busy); // delete

								blMsgSent = true;
								console.log('Test'); // delete
								var tempUserNo = ik;
								var entry  = {type: 1, user_id: users[nGroup][tempUserNo].id, argument_id: args[nGroup][addedArgNo].id};
								var query = connection.query('INSERT INTO Questions SET ?', entry, function(error, results) {
									if (error) throw error;
									console.log(results); // delete
									console.log('Question is now for ' + users[nGroup][tempUserNo].name); // delete
									var newQuID = results.insertId; 
									var qu = new Question(newQuID, qus.length, 1, addedArgNo, tempUserNo, nGroup, args, users);
									qus.push(qu);
									io.to(users[nGroup][tempUserNo].socketID).emit('Qu', qu);
									log(users[nGroup][nUser].id, ' qu type 1',nSession,connection);
									console.log('server sent ' + users[nGroup][tempUserNo].name + ' qu type ' + 1);
								});

							}

						}

					}
				}




			});

		});
	});





};



