-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: DB_2gr_1ses_6al_2pr
-- ------------------------------------------------------
-- Server version 5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Arguments`
--  

DROP TABLE IF EXISTS `Arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Arguments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `text` text NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `image_name` text NOT NULL,
  `topic_id` int(11) NOT NULL,
  /*`tema_id` int(11) NOT NULL,*/
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Arguments`
--

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socket` text,
  `session_id` int(11) NOT NULL,
  `timefullgroup` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

--
-- Table structure for table `Grouptopics`
--

DROP TABLE IF EXISTS `Grouptopics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Grouptopics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `is_finish` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

--
-- Table structure for table `Logs`
--

DROP TABLE IF EXISTS `Logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session` int(11) NOT NULL,
  `message` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Logs`
--

--
-- Table structure for table `Question_Children`
--

DROP TABLE IF EXISTS `Question_Children`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Question_Children` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Question_Children`
--

--
-- Table structure for table `Questions`
--

DROP TABLE IF EXISTS `Questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Questions`
--

--
-- Table structure for table `Chatlogs`
--

DROP TABLE IF EXISTS `Chatlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chatlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Chatlogs`
--

--
-- Table structure for table `Response_Arguments`
--

DROP TABLE IF EXISTS `Response_Arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Response_Arguments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `argument_text` text NOT NULL,
  `vote` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `Responses`
--

DROP TABLE IF EXISTS `Responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Responses`
--

--
-- Table structure for table `Sessions`
--

DROP TABLE IF EXISTS `Sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sessions`
--

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

--
-- Table structure for table `ChatVotes`
--

DROP TABLE IF EXISTS `ChatVotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChatVotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11)  NOT NULL,
  `topic_id` int(11)  NOT NULL,
  `argument_id` int(11)  NOT NULL,
  `numero` int(11)  NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChatVotes`
--

--
-- Table structure for table `Votes`
--

DROP TABLE IF EXISTS `Votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `argument_id` int(11) NOT NULL,
  `vote` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Votes`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-11 18:08:35


INSERT INTO `Users` VALUES (1,'Franco_Alfaro',1),(2,'Alberto_Leon',1),(3,'Jaime_Gonzalez',1),(4,'Matias_Rojas',2),(5,'Natasha_Mckenzie',2),(6,'Che_Copete',2),(7,'Che_Guevara',3),(8,'Miguel_Angelo',3),(9,'Juan_Muralla',3),(10,'Bob_Maliuquen',4),(11,'Maestro_splitter',4),(12,'Chin_Chan',4),(13,'Hanamishi_Sakuragi',5),(14,'Takenori_Akagi',5),(15,'Bati_Cristo',5),(16,'Condo_Rito',6),(17,'Aquiles_Baeza',6),(18,'Aquiles_Bailo',6),(19,'Rosa_Espinoza',7),(20,'Rosa_Melnuezni',7),(21,'Banana_Joe',7);


INSERT INTO `Groups` VALUES (1,NULL,1,'0'),(2,NULL,1,'0'),(3,NULL,1,'0'),(4,NULL,1,'0'),(5,NULL,1,'0'),(6,NULL,1,'0'),(7,NULL,1,'0');


INSERT INTO `Arguments` VALUES (1,0,NULL,'Tu curso se prepara para la eleccion de su directiva. Sin embargo nunca han hecho esto y quieren tener un buen sistema de elecciones. Surgen 3 ideas dentro del curso:#¿Cual te parece mejor sistema de elecciones? ¿Por que?',NULL,1,1,'godzilla',1),(2,1,1,'1.- Que la directiva sea formada por los y las estudiantes con mejores notas.',NULL,1,1,'godzilla',1),(3,1,1,'2.- Que la directiva la elija el o la profesora.',NULL,1,1,'godzilla',1),(4,1,1,'3.- Que todos y todas puedan postular para luego decidir en elecciones libres. ',NULL,1,1,'godzilla',1),(5,0,NULL,'Excepecionalmente, solo uno de tus companeros ocupa todos los roles de la directiva: Presidente, secretario y tesorero. Tu companero toma decisiones con las que no todos estan de acuerdo#¿Como se podria mejorar esta situacion? ',NULL,1,1,'godzilla',5),(6,1,5,'1.- Restituir los roles y elegir a un companero o companera para cada cargo de la directiva.  ',NULL,1,1,'godzilla',5),(7,1,5,'2.- Que todas las decisiones sean tomadas a traves de votaciones.',NULL,1,1,'godzilla',5),(8,1,5,'3.- Dejar que el companero siga tomando decisiones por si solo.',NULL,1,1,'godzilla',5),(9,0,NULL,'Tu recibes una invitacion con informacion para trabajar en propuestas para una nueva Constitucion.#¿Que piensas que hay hacer? ¿Por que? ',NULL,1,1,'godzilla',9),(10,1,9,'1.- Participar y convencer a otros de que se involucren. ',NULL,1,1,'godzilla',9),(11,1,9,'2.- Dejarlo pasar, ya alguien mas hara la ley por mi. ',NULL,1,1,'godzilla',9),(12,1,9,'3.- Informarme acerca del proceso pero dejar que pase sin participar. ',NULL,1,1,'godzilla',9),(13,0,NULL,'Tu curso se prepara para la eleccion de su directiva. Sin embargo nunca han hecho esto y quieren tener un buen sistema de elecciones. Surgen 3 ideas dentro del curso:#¿Cual te parece mejor sistema de elecciones? ¿Por que?',NULL,2,1,'godzilla',13),(14,1,13,'1.- Que la directiva sea formada por los y las estudiantes con mejores notas.',NULL,2,1,'godzilla',13),(15,1,13,'2.- Que la directiva la elija el o la profesora.',NULL,2,1,'godzilla',13),(16,1,13,'3.- Que todos y todas puedan postular para luego decidir en elecciones libres. ',NULL,2,1,'godzilla',13),(17,0,NULL,'Excepecionalmente, solo uno de tus companeros ocupa todos los roles de la directiva: Presidente, secretario y tesorero. Tu companero toma decisiones con las que no todos estan de acuerdo#¿Como se podria mejorar esta situacion? ',NULL,2,1,'godzilla',17),(18,1,17,'1.- Restituir los roles y elegir a un companero o companera para cada cargo de la directiva.  ',NULL,2,1,'godzilla',17),(19,1,17,'2.- Que todas las decisiones sean tomadas a traves de votaciones.',NULL,2,1,'godzilla',17),(20,1,17,'3.- Dejar que el companero siga tomando decisiones por si solo.',NULL,2,1,'godzilla',17),(21,0,NULL,'Tu recibes una invitacion con informacion para trabajar en propuestas para una nueva Constitucion.#¿Que piensas que hay hacer? ¿Por que? ',NULL,2,1,'godzilla',21),(22,1,21,'1.- Participar y convencer a otros de que se involucren. ',NULL,2,1,'godzilla',21),(23,1,21,'2.- Dejarlo pasar, ya alguien mas hara la ley por mi. ',NULL,2,1,'godzilla',21),(24,1,21,'3.- Informarme acerca del proceso pero dejar que pase sin participar. ',NULL,2,1,'godzilla',21),(25,0,NULL,'Tu curso se prepara para la eleccion de su directiva. Sin embargo nunca han hecho esto y quieren tener un buen sistema de elecciones. Surgen 3 ideas dentro del curso:#¿Cual te parece mejor sistema de elecciones? ¿Por que?',NULL,3,1,'godzilla',25),(26,1,25,'1.- Que la directiva sea formada por los y las estudiantes con mejores notas.',NULL,3,1,'godzilla',25),(27,1,25,'2.- Que la directiva la elija el o la profesora.',NULL,3,1,'godzilla',25),(28,1,25,'3.- Que todos y todas puedan postular para luego decidir en elecciones libres. ',NULL,3,1,'godzilla',25),(29,0,NULL,'Excepecionalmente, solo uno de tus companeros ocupa todos los roles de la directiva: Presidente, secretario y tesorero. Tu companero toma decisiones con las que no todos estan de acuerdo#¿Como se podria mejorar esta situacion? ',NULL,3,1,'godzilla',29),(30,1,29,'1.- Restituir los roles y elegir a un companero o companera para cada cargo de la directiva.  ',NULL,3,1,'godzilla',29),(31,1,29,'2.- Que todas las decisiones sean tomadas a traves de votaciones.',NULL,3,1,'godzilla',29),(32,1,29,'3.- Dejar que el companero siga tomando decisiones por si solo.',NULL,3,1,'godzilla',29),(33,0,NULL,'Tu recibes una invitacion con informacion para trabajar en propuestas para una nueva Constitucion.#¿Que piensas que hay hacer? ¿Por que? ',NULL,3,1,'godzilla',33),(34,1,33,'1.- Participar y convencer a otros de que se involucren. ',NULL,3,1,'godzilla',33),(35,1,33,'2.- Dejarlo pasar, ya alguien mas hara la ley por mi. ',NULL,3,1,'godzilla',33),(36,1,33,'3.- Informarme acerca del proceso pero dejar que pase sin participar. ',NULL,3,1,'godzilla',33),(37,0,NULL,'Excepecionalmente, solo uno de tus companeros ocupa todos los roles de la directiva: Presidente, secretario y tesorero. Tu companero toma decisiones con las que no todos estan de acuerdo#¿Como se podria mejorar esta situacion? ',NULL,4,1,'godzilla',37),(38,1,37,'1.- Restituir los roles y elegir a un companero o companera para cada cargo de la directiva.  ',NULL,4,1,'godzilla',37),(39,1,37,'2.- Que todas las decisiones sean tomadas a traves de votaciones.',NULL,4,1,'godzilla',37),(40,1,37,'3.- Dejar que el companero siga tomando decisiones por si solo.',NULL,4,1,'godzilla',37),(41,0,NULL,'Excepecionalmente, solo uno de tus companeros ocupa todos los roles de la directiva: Presidente, secretario y tesorero. Tu companero toma decisiones con las que no todos estan de acuerdo#¿Como se podria mejorar esta situacion? ',NULL,5,1,'godzilla',41),(42,1,41,'1.- Restituir los roles y elegir a un companero o companera para cada cargo de la directiva.  ',NULL,5,1,'godzilla',41),(43,1,41,'2.- Que todas las decisiones sean tomadas a traves de votaciones.',NULL,5,1,'godzilla',41),(44,1,41,'3.- Dejar que el companero siga tomando decisiones por si solo.',NULL,5,1,'godzilla',41),(45,0,NULL,'La Direccion del colegio inicio una serie de entrevistas para resolver quien cometio una falta grave entre tus companoers de curso. Tu mejor amiga o amigo fue el responsable, sin embargo no te gustaria que lo castigaran.#¿Que opcion tomas? ¿Por que? ',NULL,6,1,'godzilla',45),(46,1,45,'1.- Culpar a otro companero o companera aunque se que no es verdad.',NULL,6,1,'godzilla',45),(47,1,45,'2.- Decir la verdad, aunque sea mi amigo o amiga.',NULL,6,1,'godzilla',45),(48,1,45,'3.- Decir que no se nada, aunque puedan descubrirlo.',NULL,6,1,'godzilla',45),(49,0,NULL,'Uno de tus companeros o companeras se ha lastimado gravemente y lo han llevado a urgencia. Sin embargo no tiene seguro escolar y les han negado la atencion medica. Tuvieron que llevarlo a otro hospital, perdiento tiempo valioso para su recuperacion. #¿Te parece esta una actitud acorde a los Derechos Constitucionales? ',NULL,6,1,'godzilla',49),(50,1,49,'1.- Si, todos debiesen pagar por los servicios que usan. ',NULL,6,1,'godzilla',49),(51,1,49,'2.- Todos debiesen recibir un servicio de urgencia sin importar su nivel socioeconomico. ',NULL,6,1,'godzilla',49),(52,1,49,'3.- Esta bien que las personas sean derivadas al lugar que corresponda segun su seguro y capacidad economica.',NULL,6,1,'godzilla',49),(53,0,NULL,'La Direccion del colegio inicio una serie de entrevistas para resolver quien cometio una falta grave entre tus companoers de curso. Tu mejor amiga o amigo fue el responsable, sin embargo no te gustaria que lo castigaran.#¿Que opcion tomas? ¿Por que? ',NULL,7,1,'godzilla',53),(54,1,53,'1.- Culpar a otro companero o companera aunque se que no es verdad.',NULL,7,1,'godzilla',53),(55,1,53,'2.- Decir la verdad, aunque sea mi amigo o amiga.',NULL,7,1,'godzilla',53),(56,1,53,'3.- Decir que no se nada, aunque puedan descubrirlo.',NULL,7,1,'godzilla',53);


INSERT INTO `Grouptopics` VALUES (1,1,1,0),(2,1,5,0),(3,1,9,0),(4,2,13,0),(5,2,17,0),(6,2,21,0),(7,3,25,0),(8,3,29,0),(9,3,33,0),(10,4,37,0),(11,5,41,0),(12,6,45,0),(13,6,49,0),(14,7,53,0);


INSERT INTO `Sessions` VALUES (1,'pruebaSeeds.py');


