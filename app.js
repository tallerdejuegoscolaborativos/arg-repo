/**
 * Module dependencies.
 */



var express = require('express')
	, http = require('http')
	, path = require('path');

var app = express();

//var mysql      = require('mysql');

var MySql = require('sync-mysql');

var connection = new MySql({
	host: 'localhost',
	user: 'root',
	password: 'root' ,
    //database:'old_db',
	database:'DB_2gr_1ses_6al_2pr',
	port: 3306,
	multipleStatements:true
});

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./controllers'));

init_session(connection);

function init_session(connection){
	var server = http.createServer(app);
	require('./controllers/new_socket.js')(server, connection, 1);
	server.listen(app.get('port'), function(){
		console.log('Express server listening on port ' + app.get('port'));
	});
}